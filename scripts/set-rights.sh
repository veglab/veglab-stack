#!/bin/bash

# Create user veglab if not exist
id -u veglab &>/dev/null || useradd veglab
# Create groups veglab & docker
sudo groupadd -f veglab
sudo groupadd -f docker
# Add veglab to the groups
sudo adduser veglab veglab
sudo adduser veglab docker
# Set directories rights
sudo chown -R veglab:veglab postgres/{config,dumps,logs,shared}
sudo chmod 774 -R postgres/{config,dumps,shared}
sudo chmod 777 -R postgres/logs
