#!/bin/bash

red="\e[31m"
redBg="\e[41m"
green="\e[32m"
greenBg="\e[42m"
blue="\e[34m"
blueBg="\e[44m"
end="\e[0m"

print_usage(){
    printf "${blue}"
    printf "**********************************************************************************\n"
    printf "Utilisation: \n"
    printf "$ restore_database.sh --help\n"
    printf "\tAffiche cette aide.\n"
    printf "\n$ restore_database.sh"

    printf "\n$\t --db_ip <db_ip_address>         Adresse IP de la base de données"
    printf "\n$\t --db_name <db_name>             Nom de la base de données"
    printf "\n$\t --db_port <db_port>             Port de la base de données"
    printf "\n$\t --db_username <db_user>         Utilisateur de la base de données"
    printf "\n$\t --db_password <db_password>     Mot de passe pour l'utilisateur de la base de données"
    printf "\n$\t --dump_filename <filename>      Nom du fichier de dump qui sera placé dans /infra/data/postgres/dumps"

    printf "Restore le dump infra/postgres/dumps/<filename> dans la base de donnée <db_name> située sur le serveur <db_ip_address> et accessible via le port <db_port>\n"
    printf "\t ${redBg}IMPORTANT${end}${blue} : La base de données <db_name> doit être créée au préalable (voir le script \`create-database.sh\`\n"
    printf "\t Note : L'utilisateur doit avoir les droits nécessaires sur la base de données et les schémas\n"
    printf "**********************************************************************************"
    printf "${end}\n"
}

if [ -r .env.dev ]; then
    source .env.dev
else
  printf "${red}Ce script à besoin d'un fichier .env.dev correctement renseigné pour fonctionner.${end}"
  exit 1
fi

if [ "$1" == "help" ] || [ "$1" == "--help" ]; then
  print_usage
  exit 0
fi

if [ "$1" == "help" ] || [ "$1" == "--help" ]; then
  print_usage
  exit 0
fi


# PARAMETERS ------------------------------------------------------------------------
while [ $# -gt 0 ]; do
    if [[ $1 == "--"* ]]; then
        v="${1/--/}"
        declare "$v"="$2"
        shift
    fi
    shift
done

LOCAL_DUMP_FILENAME=${dump_filename}
DB_IP=${db_ip}
DB_NAME=${db_name}
DB_PORT=${db_port}
DB_USERNAME=${db_username}
DB_PASSWORD=${db_password}

if [[ -z "${DB_IP}" ||
    -z "${DB_NAME}"  ||
    -z "${DB_PORT}"  ||
    -z "${DB_USERNAME}"  ||
    -z "${DB_PASSWORD}"  ||
    -z "${LOCAL_DUMP_FILENAME}" ]]; then
    printf "${red}Commande erronée.${end}\n"
     print_usage
     exit 1
fi

# VARIABLES ------------------------------------------------------------------------
# Store the start date in case the script takes too long and finishes the day after
TODAY=$(date)
# Format used for all filenames beginnings (YYYYMMDD)
TODAY_FORMATTED=$(date +%Y%m%d)
# The target folder in which store the dumps in this server
HOST_DUMPS_FOLDER=$(pwd)/postgres/dumps
# The target folder in which store the logs in this server
HOST_LOGS_FOLDER=$(pwd)/logs/postgres
# The name for log and dump filename
if [ "$IMAGE_TAG" ]; then
  FILENAME_NO_EXT="${IMAGE_TAG}"_veglab
else
  FILENAME_NO_EXT="${TODAY_FORMATTED}"_veglab
fi

printf "${blue}---------------------------------------------------------------------${end}\n"
printf "${blue} Restaure une sauvegarde sur la base de données \"$DB_NAME\" ${end}\n"
printf "${blue} à partir de l'image (dump) locale : '${LOCAL_DUMP_FILENAME}' ${end}\n"
printf "${blue}---------------------------------------------------------------------${end}\n\n"

docker run -t --name "restore_bdd_veglab" \
              --workdir /home/$(id -un)/ \
              --mount type=bind,source="$HOST_DUMPS_FOLDER",target=/home/dumps/ \
              --network="veglab_back" \
              --user $(id -u):$(id -g) \
              --rm \
              postgis/postgis:15-3.3 \
              pg_restore \
              --dbname="postgresql://${DB_USERNAME}:${DB_PASSWORD}@${DB_IP}:${DB_PORT}/${DB_NAME}" \
              -c --if-exists -O --single-transaction --exit-on-error "/home/dumps/${LOCAL_DUMP_FILENAME}" \
              > "$HOST_LOGS_FOLDER/$LOCAL_DUMP_FILENAME.restore.log" 2>&1


if [ -f $HOST_LOGS_FOLDER/$LOCAL_DUMP_FILENAME.restore.log ]; then
  # Log file exists...
  if [ -s $HOST_LOGS_FOLDER/$LOCAL_DUMP_FILENAME.restore.log ]; then
    # ... and is not empty
    printf "${redBg}Impossible de restaurer la base de données. Vérifiez le fichier de log '$HOST_LOGS_FOLDER/$LOCAL_DUMP_FILENAME.restore.log'.${end}\n\n"
    exit 1
  else
    # ... and is empty
    printf "${greenBg}La base de données '${DB_NAME}' a été restaurée à partir du dump '${LOCAL_DUMP_FILENAME}'. Vérifiez le fichier de log '$HOST_LOGS_FOLDER/$LOCAL_DUMP_FILENAME.restore.log'.${end}\n"
    printf "${greenBg}(un fichier vide indique aucune erreur lors de la restauration).${end}\n\n"
  fi
else
  printf "${redBg}Impossible de restaurer la base de données.${end}\n\n"
  exit 1
fi
