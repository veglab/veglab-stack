#!/bin/bash

red="\e[30;1;41m"
green="\e[32m"
blue="\e[34m"
end="\e[0m"

print_usage(){
    printf "${blue}"
    printf "**********************************************************************************\n"
    printf "Usage: \n"
    printf "$ build-api.sh help\n"
    printf "\tShow this help.\n"
    printf "\n$ build-api.sh --env <environment> [--with-cache <image_cache>]\n\n"
    printf "\Build a Docker image of vegLab API (API Platform) for the given <environment>\n"
    printf "\and, optionally, user an existing image as cache\n"
    printf "\t <environment> : prod, preprod\n"
    printf "\t <image_cache> : registry.gitlab.com/veglab/veglab-api:preproduction\n"
    printf "**********************************************************************************"
    printf "${end}\n"
}

if [ $1 == "help" ] || [ $1 == "--help" ]; then
	print_usage
	exit 0
fi
if [ $# -lt 2 ]; then
    printf "${red}Invalid command.${end}\n"
    print_usage
    exit 1
fi
if [ $1 != "--env" ]; then
	printf "${red}Invalid command.${end}\n"
	print_usage
	exit 1
fi
if [ $2 != "prod" ] && [ $2 != "preprod" ]; then
	printf "${red}Environment \"$2\" not supported.${end}\n"
	print_usage
	exit 1
fi
WITH_CACHE=false
CACHE_IMAGE=""
if [[ $3 == "--with-cache" ]] && [ -n "$4" ]; then
	WITH_CACHE=true
	CACHE_IMAGE=$4
fi

selected_env=$2
case $selected_env in
	prod)
		image_tag=production
		;;
	preprod)
		image_tag=preproduction
		;;
esac

# If you use the --with-cache option, you should PULL the cached image first
# Uncomment to pull ; For now, pull is already done trough .gitlab-ci
#if [ "$WITH_CACHE" == true ] && [ "$CACHE_IMAGE" != '' ]; then
#	docker pull "$CACHE_IMAGE"
#fi

# (you must be logged in with docker : docker login ...)
DOCKER_BUILDKIT=1 docker build $(if [ $WITH_CACHE == true ]; then echo "--cache-from $CACHE_IMAGE"; fi) -t veglab-api:"${image_tag}" -f api/Dockerfile."${selected_env}" .
