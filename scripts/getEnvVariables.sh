#!/bin/bash
# Thanks to T. Gillet for this script

# Priority :
# .env.{prod>preprod>dev}.local > .env.{prod>preprod>dev} > .env.local > .env

red=$'\e[1;31m'

# GENERIC
# we start by getting default variables declared in .env file
# this file is required, the script fails if missing
if [ -f .env ]; then
    source .env
else
    printf "${red}Error : .env file missing. ${end}\n"
    exit 1
fi

# Then we get local overrides or additional variables in .env.local
if [ -f .env.local ]; then
    source .env.local
fi

# DEV ENV
if [ -f ".env.dev" ]; then
  source ".env.dev"
fi
if [ -f ".env.dev.local" ]; then
  source ".env.dev.local"
fi

# PREPROD ENV
if [ -f ".env.preprod" ]; then
  source ".env.preprod"
fi
if [ -f ".env.preprod.local" ]; then
  source ".env.preprod.local"
fi

# PROD ENV
if [ -f ".env.prod" ]; then
  source ".env.prod"
fi
if [ -f ".env.prod.local" ]; then
  source ".env.prod.local"
fi
