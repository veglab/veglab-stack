#!/bin/bash

red="\e[30;1;41m"
green="\e[32m"
blue="\e[34m"
end="\e[0m"

set -e

print_usage(){
    printf "${blue}"
    printf "**********************************************************************************\n"
    printf "Usage: \n"
    printf "$ build-client.sh help\n"
    printf "\tShow this help.\n"
    printf "\n$ build-client.sh --env <environment> [--with-cache <image_cache>]\n\n"
    printf "Build a Docker image of vegLab client (Angular app) for the given <environment>\n"
    printf "\and, optionally, user an existing image as cache\n"
    printf "\t <environment> : prod, preprod\n"
    printf "\t <image_cache> : registry.gitlab.com/veglab/veglab-client:preproduction\n"
    printf "**********************************************************************************"
    printf "${end}\n"
}

if [ $1 == "help" ] || [ $1 == "--help" ]; then
	print_usage
	exit 0
fi
if [ $# -lt 2 ]; then
    printf "${red}Invalid command.${end}\n"
    print_usage
    exit 1
fi
if [ $1 != "--env" ]; then
	printf "${red}Invalid command.${end}\n"
	print_usage
	exit 1
fi
if [ $2 != "prod" ] && [ $2 != "preprod" ]; then
	printf "${red}Environment \"$2\" not supported.${end}\n"
	print_usage
	exit 1
fi
WITH_CACHE=false
CACHE_IMAGE=""
if [[ $3 == "--with-cache" ]] && [ -n "$4" ]; then
	WITH_CACHE=true
	CACHE_IMAGE=$4
fi

selected_env=$2
if [ ! -f ".env.${selected_env}" ]; then
	printf "${red}You ask to build the \"${selected_env}\" environment but "
	printf "no \".env.${selected_env}\" file exists.${end}\n"
	print_usage
	exit 1
fi

case $selected_env in
	prod)
		image_tag=production
		;;
	preprod)
		image_tag=preproduction
		;;
esac

# Prepare environment
# merge environments variables from .env and .env.preprod into a new file my.local.env
awk -F= '{a[$1]=$2}END{for(i in a) if(i != "") {print i "=" a[i]}}' .env .env."${selected_env}" > my-local-build.env
sed -i '/^[[:blank:]]*#/d;s/#.*//' my-local-build.env
sort my-local-build.env

build_args=""
while read -r line; do
	build_args="$build_args --build-arg $line"
done < my-local-build.env

# If you use the --with-cache option, you should PULL the cached image first
# Uncomment to pull ; For now, pull is already done trough .gitlab-ci
#if [ "$WITH_CACHE" == true ] && [ "$CACHE_IMAGE" != '' ]; then
#	docker pull "$CACHE_IMAGE"
#fi

# Build
# (you must be logged in with docker : docker login ...)
DOCKER_BUILDKIT=1 docker build $(if [ $WITH_CACHE == true ]; then echo "--cache-from $CACHE_IMAGE"; fi)\
			 -t veglab-client:"${image_tag}"\
			 -f client/Dockerfile."${selected_env}"\
			 $build_args\
			 .

rm -f my-local-build.env
