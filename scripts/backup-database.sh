#!/bin/bash

red="\e[31m"
redBg="\e[41m"
green="\e[32m"
greenBg="\e[42m"
blue="\e[34m"
blueBg="\e[44m"
end="\e[0m"

set -e

print_usage(){
    printf "${blue}"
    printf "**********************************************************************************\n"
    printf "Utilisation: \n"
    printf "$ backup_database.sh --help\n"
    printf "\tAffiche cette aide.\n"
    printf "\n$ backup_database.sh"

    printf "\n$\t --db_ip <db_ip_address>         Adresse IP de la base de données"
    printf "\n$\t --db_name <db_name>             Nom de la base de données"
    printf "\n$\t --db_port <db_port>             Port de la base de données"
    printf "\n$\t --db_username <db_user>         Utilisateur de la base de données"
    printf "\n$\t --db_password <db_password>     Mot de passe pour l'utilisateur de la base de données"
    printf "\n$\t --tag <tag_name>                Nom du tag associé (Tag de la release Gitlab le cas échéant)"

    printf "\tCréé un dump de la base de donnée <db_name> située sur le serveur <db_ip_address> et accessible via le port <db_port>\n"
    printf "\tCe dump est sauvegardé localement, dans le dossier postgres/dumps\n\n"
    printf "\tNote : L'utilisateur doit avoir les droits nécessaires sur la base de données et les schémas\n\n"
    printf "\tNote : Si <tag_name> est fourni, le dump sera nommé \"<tag_name>_veglab\", sinon il sera nommé YYYYmmdd_veglab\n"
    printf "**********************************************************************************"
    printf "${end}\n"
}

if [ -r .env.dev ]; then
    source .env.dev
else
  printf "${red}Ce script à besoin d'un fichier .env.dev correctement renseigné pour fonctionner.${end}"
  exit 1
fi

if [ "$1" == "help" ] || [ "$1" == "--help" ]; then
  print_usage
  exit 0
fi

# PARAMETERS ------------------------------------------------------------------------
while [ $# -gt 0 ]; do
    if [[ $1 == "--"* ]]; then
        v="${1/--/}"
        declare "$v"="$2"
        shift
    fi
    shift
done

DB_IP=${db_ip}
DB_NAME=${db_name}
DB_PORT=${db_port}
DB_USERNAME=${db_username}
DB_PASSWORD=${db_password}
IMAGE_TAG=${tag}

if [[ -z "${DB_IP}" ||
    -z "${DB_NAME}"  ||
    -z "${DB_PORT}"  ||
    -z "${DB_USERNAME}"  ||
    -z "${DB_PASSWORD}" ]]; then
    printf "${red}Commande erronée.${end}\n"
     print_usage
     exit 1
fi

# VARIABLES ------------------------------------------------------------------------
# Store the start date in case the script takes too long and finishes the day after
TODAY=$(date)
# Format used for all filenames beginnings (YYYYMMDD)
TODAY_FORMATTED=$(date +%Y%m%d)
# The target folder in which store the dumps in this server
HOST_DUMPS_FOLDER=$(pwd)/postgres/dumps
# The target folder in which store the logs in this server
HOST_LOGS_FOLDER=$(pwd)/logs/postgres
# The name for log and dump filename
if [ "$IMAGE_TAG" ]; then
  FILENAME_NO_EXT="${IMAGE_TAG}"_veglab
else
  FILENAME_NO_EXT="${TODAY_FORMATTED}"_veglab
fi

printf "${blue}---------------------------------------------------------------------${end}\n"
printf "${blue} Créer une sauvegarde de la base de données \"$DB_NAME\" ${end}\n"
printf "${blue} sur le serveur PostgreSQL '${DB_IP}' ${end}\n"
printf "${blue} L'image (dump) de la base de données sera situé ici : ${end}\n"
printf "${blue}  \`${HOST_DUMPS_FOLDER}/${FILENAME_NO_EXT}.dump\` ${end}\n"
# printf "${blue} Attention : le schéma \`public\` ne fait pas partie du processus de sauvegarde${end}\n"
printf "${blue}---------------------------------------------------------------------${end}\n\n"

# SETUP ------------------------------------------------------------------------
# Check if today files already exists and delete them if they do
rm -f "${HOST_DUMPS_FOLDER}/${FILENAME_NO_EXT}".*

# GENERATE DUMP ----------------------------------------------------------------
docker run -t --name backup_bdd_veglab \
			  --workdir /home/dumps \
              --mount type=bind,source="${HOST_DUMPS_FOLDER}",target=/home/dumps/ \
              --network="veglab_back" \
              --rm \
              --user $(id -u):$(id -g) \
              postgis/postgis:15-3.3 \
              pg_dump -Fc \
              --dbname="postgresql://${DB_USERNAME}:${DB_PASSWORD}@${DB_IP}:${DB_PORT}/${DB_NAME}" -C -c --if-exists \
              -f "${FILENAME_NO_EXT}.dump" > "${HOST_LOGS_FOLDER}/${FILENAME_NO_EXT}.dump.log"

if [ -f "$HOST_LOGS_FOLDER"/"$FILENAME_NO_EXT".dump.log ]; then
  # Log file exists...
  if [ -s "$HOST_LOGS_FOLDER"/"$FILENAME_NO_EXT".dump.log ]; then
    # ... and is not empty
    printf "${redBg}Impossible de sauvegarder la base de données. Vérifiez le fichier de log '"$HOST_LOGS_FOLDER"/"$FILENAME_NO_EXT".dump.log'.${end}\n\n"
    exit 1
  else
    # ... and is empty
    printf "${greenBg}La base de données '${DB_NAME}' a été sauvegardée. Vérifiez le fichier de log '"$HOST_LOGS_FOLDER"/"$FILENAME_NO_EXT".dump.log'.${end}\n"
    printf "${greenBg}(un fichier vide indique aucune erreur lors de la sauvegarde).${end}\n\n"
  fi
else
  printf "${redBg}Impossible de sauvegarder la base de données.${end}\n\n"
  exit 1
fi
