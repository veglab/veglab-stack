#!/bin/bash

source ./scripts/getEnvVariables.sh

# Create roles
docker exec vl-postgres psql -d "${DATABASE_NAME}" -U "${POSTGRES_USER}" -c "CREATE ROLE vl_admin LOGIN SUPERUSER PASSWORD '${POSTGRES_ADMIN_PASSWORD}';"

# Create database
# (Database should already exists : it is created at vl-postgres container 1st startup)
docker exec vl-postgres psql -d "${DATABASE_NAME}" -U "${POSTGRES_USER}" -c "CREATE DATABASE ${DATABASE_NAME} WITH OWNER vl_admin;"

# Create schema
docker exec vl-api php bin/console doctrine:schema:update --force
