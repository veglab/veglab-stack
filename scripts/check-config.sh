#!/bin/bash

red="\033[0;31m"
green="\e[32m"
yellow="\033[0;33m"
blue="\033[0;34m"
end="\e[0m"

check_symbol='\u2714'
danger_symbol=''
cross_symbol='\u274c'

runningInstances=($(docker ps | grep -c 'vl-.*\w'))
if [ "$runningInstances" -eq 0 ]; then
    printf "${red}---- Set up docker services before running this script.${end}"
    exit 1
fi

DUMP_ENV=false
if [ $# -eq 1 ] && [ "$1" == "--dump-values" ]; then
    DUMP_ENV=true
fi

# Environment variables to be checked
hostsEnvVar=(DOMAIN CLIENT_SUBDOMAIN TRAEFIK_SUBDOMAIN API_SUBDOMAIN ES_SUBDOMAIN SSO_SUBDOMAIN REPOS_API_SUBDOMAIN R_PLUMBER_SUBDOMAIN DOCS_SUBDOMAIN)
postgresEnvVar=(POSTGRES_PASSWORD POSTGRES_DB POSTGRES_USER)
apiEnvVar=(APP_ENV POSTGRES_HOST POSTGRES_USER POSTGRES_PASSWORD \
           POSTGRES_INTERNAL_PORT POSTGRES_ADMIN_PASSWORD POSTGRES_DATABASE_NAME \
           POSTGRES_VERSION KEYCLOAK_REALM KEYCLOAK_URL KEYCLOAK_CLIENT_ID FOS_ELASTICA_PASSWORD \
           FOS_ELASTICA_INDEXES_USERS_INDEX_NAME FOS_ELASTICA_INDEXES_OCCURRENCES_INDEX_NAME \
           FOS_ELASTICA_INDEXES_SYNTHETIC_COLUMNS_INDEX_NAME FOS_ELASTICA_INDEXES_TABLES_INDEX_NAME \
           FOS_ELASTICA_INDEXES_OBSERVERS_INDEX_NAME FOS_ELASTICA_INDEXES_BIBLIO_PHYTO_INDEX_NAME)
clientEnvVar=(CLIENT_HOST API_HOST ES_HOST ELASTIC_PASSWORD ES_REPO_HOST SSO_HOST GEO_API_HOST R_API_HOST \
              SSO_URI KEYCLOAK_REALM SSO_CLIENT_ID SSO_LOGIN_ENDPOINT SSO_LOGOUT_ENDPOINT SSO_REFRESH_ENDPOINT \
              SSO_REFRESH_INTERVAL SSO_ROLE_ADMIN MAPQUEST_API_KEY \
              FOS_ELASTICA_INDEXES_USERS_INDEX_NAME FOS_ELASTICA_INDEXES_OCCURRENCES_INDEX_NAME \
              FOS_ELASTICA_INDEXES_SYNTHETIC_COLUMNS_INDEX_NAME FOS_ELASTICA_INDEXES_TABLES_INDEX_NAME \
              FOS_ELASTICA_INDEXES_OBSERVERS_INDEX_NAME FOS_ELASTICA_INDEXES_BIBLIO_PHYTO_INDEX_NAME)
#
#
checkDockerServiceEnvVar() {
    DOCKER_SERVICE=$1
    declare -a ENV_ARRAY=("${!2}")

    local -n DOCKER_ENV_VAR_ERRORS=$3

    printf "${blue}$DOCKER_SERVICE:${end}\n"

    IS_SERVICE_RUNNING=$(docker ps | grep -c "${DOCKER_SERVICE}")
    if [ "${IS_SERVICE_RUNNING}" -eq 0 ]; then
        printf "  ${red}${cross_symbol} Service ${DOCKER_SERVICE} is not running${end}\n"
    else
      for item in "${ENV_ARRAY[@]}"
      do
          ENV_VAR="$item"
          ENV_VAR_VALUE=$(docker exec "$DOCKER_SERVICE" env | grep "$ENV_VAR")
          ENV_VALUE=$(echo $ENV_VAR_VALUE | cut -d'=' -f2)

          ENV_VAR_EXIST=false
          ENV_VALUE_IS_EMPTY=true

          PRINT_COLOR="${red}"
          PRINT_SYMBOL="${cross_symbol}"

          if [ "$ENV_VAR_VALUE" != "" ]; then
              ENV_VAR_EXIST=true
              PRINT_COLOR="${yellow}"
              PRINT_SYMBOL="${cross_symbol}"
              if [ "$ENV_VALUE" != "" ]; then
                  ENV_VALUE_IS_EMPTY=false
                  PRINT_COLOR="${green}"
                  PRINT_SYMBOL="${check_symbol}"
              else
                  PRINT_COLOR="${red}"
                  PRINT_SYMBOL="${cross_symbol}"
                  DOCKER_ENV_VAR_ERRORS+=("$item")
              fi
          else
              ENV_VAR_EXIST=false
              PRINT_COLOR="${red}"
              PRINT_SYMBOL="${cross_symbol}"
              DOCKER_ENV_VAR_ERRORS+=("$item")
          fi

          if [ "$DUMP_ENV" ]; then
              printf "${PRINT_COLOR}  ${PRINT_SYMBOL} "
              if [ "$ENV_VAR_EXIST" == false ]; then
                  echo \""$ENV_VAR"\" DOES NOT EXIST !
              else
                  if [ "$ENV_VALUE_IS_EMPTY" == "true" ]; then
                      echo \""$ENV_VAR"\" EXISTS BUT IS EMPTY !
                  else
                      if [ "$DUMP_ENV" == "true" ]; then
                          echo "$ENV_VAR=$ENV_VALUE"
                      else
                          echo "$ENV_VAR"
                      fi
                  fi
              fi
              printf "${end}"
          fi
      done
    fi
}

# ----------------------------------
# CHECK DOCKER ENVIRONMENT VARIABLES
# ----------------------------------
printf "${blue} ---------------------------------------------- ${end}\n"
printf "${blue} Check Docker environment variables per service ${end}\n"
printf "${blue} ---------------------------------------------- ${end}\n"
errorArray=()
checkDockerServiceEnvVar vl-postgres postgresEnvVar[@] errorArray
checkDockerServiceEnvVar vl-api apiEnvVar[@] errorArray
checkDockerServiceEnvVar vl-client clientEnvVar[@] errorArray
hasError=false
if [ ${#errorArray[@]} -gt 0 ]; then
    hasError=true
fi

if [ $hasError == "true" ]; then
    printf "\n${red}Undefined or empty environment variables:${end}\n"
    for itemError in "${errorArray[@]}"
    do
        printf "${red}  ${cross_symbol} $itemError${end}\n"
    done
    exit 1
else
    exit 0
fi


