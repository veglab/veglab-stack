# Concepts

Nous présentons ici les principaux concepts utilisés dans l'application.  

- [Occurrence](Occurrence.md)
- [Tableau](Tableau.md)
- [Groupe de lignes](Groupe-de-lignes.md)
- [Groupe de colonnes](Groupe-de-colonnes-SyE.md)
- [Identification](Identification.md)
- [Champ étendu](Champ-etendu.md)
