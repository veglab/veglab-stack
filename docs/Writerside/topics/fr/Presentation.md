# Présentation

VegLab est une plateforme de traitement et d'analyse de données pour les végétations et les habitats naturels à déstination des phytosociologues et des écologues.

L'analyse de données de végétations recourt à de nombreuses techniques et outils qui diffèrent selon les personnes, les écoles et les pays.

L'objectif de VegLab est de fournir une série d'**outils _had oc_** pour le traitement de données (relevés et tableaux phytosociologiques) ainsi qu'un **cadre de travail** pour l'écologue et le phytosociologue intégrant au mieux les différentes étapes de son flux de travail.

Ainsi, le but de la plateforme n'est pas de contraindre ou de limiter à une seule approche le travail de l'écologue et du phytosociologue, mais d'offrir une palette d'outils et de processus permettant d'améliorer le traitement et l'analyse de ces données.

## Outils
- Référentiels taxonomiques, phytosociologiques, d'habitats naturels et de traits d'histoire de vie intégrés et évolutifs.
- Modules de recherche de relevés et de tableaux.
- Création et manipulation de tableaux au format tableur.
- Gestion des identifications.
- Interfaces cartographiques.
- Serveur R intégré.
- Import et export au format tableur.

## Cadre de travail
- Modèle de données et interfaces centrés sur le tableau phytosociologique.
- Capacités depuis la saisie ou l'import de données jusqu'à la publication.
- Capitalisation des tableaux mis en forme.
- Phytosociologie sigmatiste ou synusiale.
- Facilités d'échanges entre utilisateurs.
- Échange de données avec d'autres systèmes d'information.
