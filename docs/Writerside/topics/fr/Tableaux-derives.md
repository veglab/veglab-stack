# Tableaux dérivés

Lorsque vous travaillez sur un tableau phytosociologique, vous souhaitez parfois disposer de différentes versions 
de travail de ce tableau ou présenter différentes approches de votre travail.
Lorsqu'un autre utilisateur souhaite réutiliser l'un de vos tableaux, il ne peut pas modifier directement l'un de vos 
tableaux et doit en faire une copie de travail.

Les tableaux dérivés sont un moyen de conserver la trace des différentes évolutions ayant eu lieu à partir d'un tableau source. 
Ces évolutions peuvent avoir été apportées par vous-même ou par un autre utilisateur.
Le tableau initial ainsi que l'ensemble des tableaux dérivés forment une arborescence que vous pouvez consulter via 
le menu ![icône arborescence des tableaux dérivés](../../images/icons/alt_route.png) "Arborescence des tableaux dérivés".

![Arborescence des tableaux dérivés](../../images/table_tree/table_tree.png)

_Description des éléments à partir d'un exemple d'arborescence
contenant un tableau initial et deux tableaux dérivés._ 

## Créer un tableau dérivé

Pour créer un nouveau tableau dérivé à partir d'un tableau source, il suffit de ![icône dupliquer](../../images/icons/file_copy.png) dupliquer le tableau source.  

> Lorsque vous souhaitez utiliser un tableau dont vous n'êtes pas l'auteur, vous devez auparavant le dupliquer afin d'en faire une copie de travail.

Le tableau dérivé a les mêmes caractéristiques et la même mise en forme que le tableau source **à un instant donné** mais vous en êtes le propriétaire.  
Vous pouvez travailler sur ce nouveau tableau comme avec n'importe quel autre tableau (ajout de relevés, de groupes, etc.).  
Si le tableau source est modifié par la suite, vous ne bénéficieraient pas automatiquement de ces évolutions.  

## Cas d'utilisation
- Conserver un historique de vos versions de travail.
- Présenter votre tableau avec différentes approches.
- Mettre à disposition une version spécifique de votre tableau.
- Obligatoirement si vous souhaitez utiliser le tableau d'un autre utilisateur.

## Supprimer un tableau dérivé
Vous pouvez à tout moment décider de supprimer l'un de vos tableaux (LIEN VERS LA DOCUMENTATION).  
Si ce tableau est lié à un ou plusieurs tableaux dérivés, ceux-ci **ne sont pas supprimés** mais sont 
automatiquement rattachés au tableau parent le plus proche.  

![Supprimer un tableau dérivé](../../images/table_tree/table_tree_delete_table.png)

Dans le cas où le tableau supprimé est le tableau initial (c'est-à-dire la source d'un ensemble de tableaux dérivés), 
les tableaux directement liés ("enfants directs") sont alors traités comme des tableaux initiaux.  
Dans l'exemple ci-dessus, la suppression du tableau n° 137 ferait du tableau n°138 la source de tous les tableaux 
dérivés (n°139 dans l'exemple). 
