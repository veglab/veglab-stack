# Occurrence

## Définition
Une occurrence est la représentation d’une donnée relevée sur le terrain ou issue de la bibliographie.  

Il peut s'agir d'un **individu d'espèce** ou d'un **individu de végétation**.  On parle alors d'une **occurrence de taxon** 
ou d'une **occurrence de syntaxon**.  

Les principales informations liées à une occurrence sont toujours les mêmes, quel que soit le niveau d'intégration de l'occurrence :
- observateur
- date d'observation
- localisation
- identification(s)
- niveau d'intégration

> VegLab ne gère pas de "relevé" à proprement parler, mais uniquement des occurrences de syntaxon.
> Les informations traditionnellement liées au relevé sont prises en charge par les ["champs étendus"](Champ-etendu.md).
>
> Par souci de simplicité, nous utilisons les termes courants "espèce" ou "taxon" pour les occurrences de taxon et
> "relevé" pour les occurrences de syntaxon.

## Intégration {collapsible="true"} {id="occurrence-integration"}

Les trois niveaux d'intégration actuellement gérés par VegLab sont les suivants :

- Microcénose
  - Synusie
    - Idiotaxon 

> Sur la définition de la microcénose.  
> 
> _"[...] il y a lieu d'identifier un niveau d'intégration intermédiaire entre synusie
et phytocénose, qui correspond au niveau d'intégration de la phytosociologie sigmatiste "à la
française" [...]. Nous proposons de retenir pour ce niveau d'intégration le terme de microcénose 
> proposé par Barkman (1973) dans un sens proche. Les niveaux d'intégration de la phytosociologie sont alors : 
> synusie, microcénose, phytocénose, tessela, caténa."_
> 
> [_Catteau & Duhamel 2011_](https://bibli.cbnbl.org/index.php?lvl=notice_display&id=586321) 

> Le concept de microcénose est particulièrement intéressant : il nous permet de traiter les données issues de la
> phytosociologie sigmatiste ou synusiale selon le même modèle de données.
{style="note"}

> Vous n'avez pas à connaitre le fonctionnement des niveaux d'intégration pour utiliser l'application. 
> Il s'agit de concepts utilisés "en interne".
{style="note"}

L'image ci-dessous représente la modélisation d'une occurrence de syntaxon (relevé) contenant des occurrences 
d'idiotaxons (espèces, taxons) et leur visualisation dans un tableau phytosociologique.  


![occurrence_synusy_idiotaxon_table.png](occurrence_synusy_idiotaxon_table.png)

Dans le cas d'une occurrence de syntaxon de niveau "microcénose", la représentation devient  

![occurrence_microcenosis_synusy_idiotaxon_table.png](occurrence_microcenosis_synusy_idiotaxon_table.png)
