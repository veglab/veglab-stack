import { IMain, IDatabase } from 'pg-promise';
import { configDotenv } from 'dotenv';
import pgPromise from 'pg-promise';

configDotenv({path: '/repos-api/.env'});

const pgp: IMain = pgPromise();

export const veglabDbConfig: any = {
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_INTERNAL_PORT,
    database: process.env.POSTGRES_DATABASE_NAME,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD
}

export const veglabDb: IDatabase<any> = pgp(veglabDbConfig);
