export interface Iso3166Model {
    ref: 'ISO_3166-1' | 'ISO_3166-2';
    code: string;
    level: string;
    parentCode: string;
    nameFr: string;
    nameEn: string;
}
