export interface PVF2 {
    cd_hab: number;
    fg_validite: string;
    cd_typo: number;
    lb_code: string;
    lb_hab_fr: string;
    lb_hab_fr_complet: string;
    lb_hab_en: string;
    lb_auteur: string;
    niveau: number;
    lb_niveau: string;
    cd_hab_sup: number;
    path_cd_hab: string;
    france: boolean;
    lb_description: string;
}
