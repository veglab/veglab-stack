export interface CatVeg {
    cd_nom: number;
    cd_sup: number;
    rang: number;
    lb_nom: string;
    autorites: string;
    pp: string;
    remarques_nomenc: string;
}
