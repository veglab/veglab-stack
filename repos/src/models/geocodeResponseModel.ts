export interface GeocodeResponseModel {
    levenshtein_distance: number;
    id: number,
    name: string;
    countryCode: string;
    country: string;
    regionCode: string;
    region: string;
    countyCode: string;
    county: string;
    cityCode: string;
    city: string;
    postcode: string;
    geojson: {
        type: string;
        coordinates: any;
    };
    centroid: {
        type: 'Point';
        coordinates: any;
    };
}
