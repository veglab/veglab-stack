export interface ValidateErrorJSONModel {
    message: "Validation failed";
    details: { [name: string]: unknown };
}
