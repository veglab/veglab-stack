export interface ParameterErrorModel {
    message: "Wrong parameters";
    details: { [name: string]: unknown };
}
