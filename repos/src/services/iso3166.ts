import * as db from '../config/database';
import { Iso3166Model } from "../models/iso3166Model";

export function getCountries() {
    const query = `
        SELECT
            ref,
            code,
            level,
            parent_code as parentCode,
            name_fr as nameFr,
            name_en as nameEn
        FROM geo.ref_iso3166 r
        WHERE r.level = 'country'
        ORDER BY name_fr;
        `;

    return db.veglabDb.manyOrNone<Iso3166Model>(query);
}

export function getRegions(countryCode: string) {
    const countries = countryCode.split(',').map(c => "'" + c + "'").join(',');
    const query = `
        SELECT
            ref,
            code,
            level,
            parent_code as parentCode,
            name_fr as nameFr,
            name_en as nameEn
        FROM geo.ref_iso3166 r
        WHERE r.level = 'region' AND r.parent_code IN (${countries})
        ORDER BY name_fr;
        `;

    return db.veglabDb.manyOrNone<Iso3166Model>(query);
}

export function getCounties(regionCode: string) {
    const regions = regionCode.split(',').map(r => "'" + r + "'").join(',');
    const query = `
        SELECT
            ref,
            code,
            level,
            parent_code as parentCode,
            name_fr as nameFr,
            name_en as nameEn
        FROM geo.ref_iso3166 r
        WHERE r.level = 'county' AND r.parent_code IN (${regions})
        ORDER BY name_fr;
        `;

    return db.veglabDb.manyOrNone<Iso3166Model>(query);
}

export function getCities(countyCode: string) {
    const counties = countyCode.split(',').map(c => "'" + c + "'").join((','));
    const query = `
        SELECT
            id,
            name,
            level,
            country_code AS countryCode,
            region_code AS regionCode,
            county_code AS countyCode,
            city_code AS cityCode,
            postcode AS postCode
        FROM geo.boundary b
        WHERE b.level = 'city' AND b.county_code IN (${counties})
        ORDER BY name;
        `;

    return db.veglabDb.manyOrNone<Iso3166Model>(query);
}

export function getCode(code: string) {
    const query = `
        SELECT
            ref,
            code,
            level,
            parent_code as parentCode,
            name_fr as nameFr,
            name_en as nameEn
        FROM geo.ref_iso3166 r
        WHERE r.code = '${code}';
        `;

    return db.veglabDb.manyOrNone<Iso3166Model>(query);
}
