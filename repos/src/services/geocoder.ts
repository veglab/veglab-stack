import * as db from '../config/database';
import { ReverseGeocodeResponseModel } from '../models/reverseGeocodeResponseModel';
import { GeocodeResponseModel } from "../models/geocodeResponseModel";
import { ParameterError } from "../errors/parameterError";

export function reverse(lat: number, lng: number) {
    const query = `
        SELECT
            b.id,
            name,
            b.level,
            country_code AS countryCode,
            country.name_fr AS country,
            region_code AS regionCode,
            region.name_fr AS region,
            county_code AS countyCode,
            county.name_fr AS county,
            city_code AS cityCode,
            CASE
                WHEN b.level = 'city' THEN name
                ELSE ''
            END AS city,
            postcode,
            geojson,
            geojson_centroid AS centroid
        FROM geo.boundary b
        LEFT JOIN geo.ref_iso3166 country ON country.code = country_code
        LEFT JOIN geo.ref_iso3166 region ON region.code = region_code
        LEFT JOIN geo.ref_iso3166 county ON county.code = county_code
        WHERE st_within(ST_GEOMFROMTEXT('POINT(${lng} ${lat})', 4326), b.the_geom_4326);
    `;

    return db.veglabDb.oneOrNone<ReverseGeocodeResponseModel>(query);
}

export function geocode(term: string, countryCode?: string, regionCode?: string, countyCode?: string, level?: 'country'|'region'|'county'|'city') {
    const limitResults = 10;
    let whereClause = '';
    if (countryCode || regionCode || countyCode || level) {
        if (!countryCode && !regionCode && !countyCode && level) {
            whereClause = `WHERE b.level = '${level}'`;
        } else if (countryCode && !regionCode && !countyCode && !level) {
            whereClause = `WHERE b.country_code = '${countryCode}'`;
        } else if (countryCode && !regionCode && !countyCode && level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.level = '${level}'`;
        } else if (!countryCode && regionCode && !countyCode && !level) {
            whereClause = `WHERE b.region_code = '${regionCode}'`;
        } else if (!countryCode && regionCode && !countyCode && level) {
            whereClause = `WHERE b.region_code = '${regionCode}' AND b.level = '${level}'`;
        } else if (!countryCode && !regionCode && countyCode && !level) {
            whereClause = `WHERE G.county_code = '${countyCode}'`;
        } else if (!countryCode && !regionCode && countyCode && level) {
            whereClause = `WHERE b.county_code = '${countyCode}' AND b.level = '${level}'`;
        }  else if (countryCode && regionCode && !countyCode && !level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}'`;
        } else if (countryCode && regionCode && !countyCode && level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}' AND b.level = '${level}'`;
        } else if (!countryCode && regionCode && countyCode && !level) {
            whereClause = `WHERE b.region_code = '${regionCode}' AND b.county_code = '${countyCode}'`;
        } else if (!countryCode && regionCode && countyCode && level) {
            whereClause = `WHERE b.region_code = '${regionCode}' AND b.county_code = '${countyCode}' AND b.level = '${level}'`;
        } else if (countryCode && !regionCode && countyCode && !level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.county_code = '${countyCode}'`;
        } else if (countryCode && !regionCode && countyCode && level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.county_code = '${countyCode}' AND b.level = '${level}'`;
        } else if (countryCode && regionCode && countyCode && !level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}' AND b.county_code = '${countyCode}'`;
        } else if (countryCode && regionCode && countyCode && level) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}' AND b.county_code = '${countyCode}' AND b.level = '${level}'`;
        }
    }

    const escapedTerm = term.replace(/'/g, "''");

    const query = `
        SELECT
            levenshtein(lower(unaccent(name)), lower(unaccent('${escapedTerm}')))::int AS levenshtein_distance,
            b.id,
            name,
            b.level,
            country_code AS countryCode,
            country.name_fr AS country,
            region_code AS regionCode,
            region.name_fr AS region,
            county_code AS countyCode,
            county.name_fr AS county,
            city_code AS cityCode,
            CASE
                WHEN b.level = 'city' THEN name
                ELSE ''
            END AS city,
            postcode,
            geojson,
            geojson_centroid AS centroid
        FROM geo.boundary b
        LEFT JOIN geo.ref_iso3166 country ON country.code = country_code
        LEFT JOIN geo.ref_iso3166 region ON region.code = region_code
        LEFT JOIN geo.ref_iso3166 county ON county.code = county_code
        ${whereClause && whereClause !== '' ? whereClause : ''}
        ORDER BY levenshtein_distance ASC
        LIMIT ${limitResults};
    `;
    console.log(query);

    return db.veglabDb.manyOrNone<GeocodeResponseModel>(query);
}

export function validate(countryCode?: string, regionCode?: string, countyCode?: string, cityCode?: string) {
    const limitResults = 10;
    let whereClause = '';

    const level = cityCode ? 'city' : countyCode ? 'county' : regionCode ? 'region' : countryCode ? 'country' : null;

    if (level && (countryCode || regionCode ||countyCode || cityCode)) {
        if (countryCode && !regionCode && !countyCode && !cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}'`;
        } else if (!countryCode && regionCode && !countyCode && !cityCode) {
            whereClause = `WHERE b.region_code = '${regionCode}'`;
        } else if (!countryCode && !regionCode && countyCode && !cityCode) {
            whereClause = `WHERE b.county_code = '${countyCode}'`;
        } else if (!countryCode && !regionCode && !countyCode && cityCode) {
            throw new ParameterError('You must provide a country, region or county code alongside the city code');
        } else if (countryCode && regionCode && !countyCode && !cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}'`;
        } else if (!countryCode && regionCode && countyCode && !cityCode) {
            whereClause = `WHERE b.region_code = '${regionCode}' AND b.county_code = '${countyCode}'`;
        } else if (countryCode && !regionCode && countyCode && !cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.county_code = '${countyCode}'`;
        } else if (countryCode && regionCode && countyCode && !cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}' AND b.county_code = '${countyCode}'`;
        } else if (countryCode && !regionCode && !countyCode && cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.city_code = '${cityCode}'`;
        }  else if (!countryCode && regionCode && !countyCode && cityCode) {
            whereClause = `WHERE b.region_code = '${regionCode}' AND b.city_code = '${cityCode}'`;
        } else if (!countryCode && !regionCode && countyCode && cityCode) {
            whereClause = `WHERE b.county_code = '${countyCode}' AND b.city_code = '${cityCode}'`;
        } else if (countryCode && regionCode && !countyCode && cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}' AND b.city_code = '${cityCode}'`;
        } else if (!countryCode && regionCode && countyCode && cityCode) {
            whereClause = `WHERE b.region_code = '${regionCode}' AND b.county_code = '${countyCode}' AND b.city_code = '${cityCode}'`;
        } else if (countryCode && !regionCode && countyCode && cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.county_code = '${countyCode}' AND b.city_code = '${cityCode}'`;
        }  else if (countryCode && regionCode && countyCode && cityCode) {
            whereClause = `WHERE b.country_code = '${countryCode}' AND b.region_code = '${regionCode}' AND b.county_code = '${countyCode}' AND b.city_code = '${cityCode}'`;
        }
    } else {
        throw new Error('You must provide a country, region, county or city code');
    }

    const query = `
        SELECT
            b.id,
            name,
            b.level,
            country_code AS countryCode,
            country.name_fr AS country,
            region_code AS regionCode,
            region.name_fr AS region,
            county_code AS countyCode,
            county.name_fr AS county,
            city_code AS cityCode,
            CASE
                WHEN b.level = 'city' THEN name
                ELSE ''
            END AS city,
            postcode
        FROM geo.boundary b
        LEFT JOIN geo.ref_iso3166 country ON country.code = country_code
        LEFT JOIN geo.ref_iso3166 region ON region.code = region_code
        LEFT JOIN geo.ref_iso3166 county ON county.code = county_code
        LEFT JOIN geo.ref_iso3166 city ON city.code = city_code
        ${whereClause && whereClause !== '' ? whereClause : ''}
        AND b.level = '${level}'
        LIMIT ${limitResults};
    `;

    return db.veglabDb.manyOrNone<GeocodeResponseModel>(query);
}
