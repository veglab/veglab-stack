import { veglabDb } from '../../config/database';
import { Eunis } from '../../models/repository/eunisModel';

export function search(term: string) {
    term = term.replace(/ +/g, '%').replace(/-/g, '%');
    const query = `
        SELECT
            *
        FROM repos.eunis EUNIS
        WHERE unaccent(EUNIS.lb_hab_fr) ILIKE unaccent('${term}%')
        OR (EUNIS.lb_code) ILIKE unaccent('${term}%')
        ;
    `;
console.log(query);
    return veglabDb.manyOrNone<Eunis>(query);
}

export function getByIdNomen(idNomen: number) {
    const query = `
        SELECT
            *
        FROM repos.eunis EUNIS
        WHERE EUNIS.cd_hab = ${idNomen};
    `;
    return veglabDb.oneOrNone<Eunis>(query);
}

export function getByIdsNomen(idsNomen: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.eunis EUNIS
        WHERE EUNIS.cd_hab IN (${idsNomen.join(",")});
    `;
    return veglabDb.manyOrNone<Eunis>(query);
}

export function getByIdTaxo(idTaxo: number) {
    const query = `
        SELECT
            *
        FROM repos.eunis EUNIS
        WHERE EUNIS.cd_hab = ${idTaxo};
    `;
    return veglabDb.oneOrNone<Eunis>(query);
}

export function getByIdsTaxo(idsTaxo: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.eunis EUNIS
        WHERE EUNIS.cd_hab IN (${idsTaxo.join(",")});
    `;
    return veglabDb.manyOrNone<Eunis>(query);
}
