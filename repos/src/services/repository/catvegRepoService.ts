import { veglabDb } from "../../config/database";
import { CatVeg } from "../../models/repository/catvegModel";

export function search(term: string) {
    term = term.replace(/ +/g, '%').replace(/-/g, '%');
    const query = `
        SELECT
            *
        FROM repos.catveg CATVEG
        WHERE unaccent(CATVEG.lb_nom || ' ' || CATVEG.autorites) ILIKE unaccent('${term}%');
    `;

    return veglabDb.manyOrNone<CatVeg>(query);
}

export function getByIdNomen(idNomen: number) {
    const query = `
        SELECT
            *
        FROM repos.catveg CATVEG
        WHERE CATVEG.cd_nom = ${idNomen};
    `;
    return veglabDb.oneOrNone<CatVeg>(query);
}

export function getByIdsNomen(idsNomen: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.catveg CATVEG
        WHERE CATVEG.cd_nom IN (${idsNomen.join(",")});
    `;
    return veglabDb.manyOrNone<CatVeg>(query);
}

export function getByIdTaxo(idTaxo: number) {
    const query = `
        SELECT
            *
        FROM repos.catveg CATVEG
        WHERE CATVEG.cd_nom = ${idTaxo};
    `;
    return veglabDb.oneOrNone<CatVeg>(query);
}

export function getByIdsTaxo(idsTaxo: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.catveg CATVEG
        WHERE CATVEG.cd_nom IN (${idsTaxo.join(",")});
    `;
    return veglabDb.manyOrNone<CatVeg>(query);
}
