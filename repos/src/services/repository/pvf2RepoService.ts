import { veglabDb } from "../../config/database";
import { PVF2 } from "../../models/repository/pvf2Model";

export function search(term: string) {
    term = term.replace(/ +/g, '%').replace(/-/g, '%');
    const query = `
        SELECT
            *
        FROM repos.pvf2 PVF2
        WHERE unaccent(PVF2.lb_hab_fr) ILIKE unaccent('${term}%');
    `;

    return veglabDb.manyOrNone<PVF2>(query);
}

export function getByIdNomen(idNomen: number) {
    const query = `
        SELECT
            *
        FROM repos.pvf2 PVF2
        WHERE PVF2.cd_hab = ${idNomen};
    `;
    return veglabDb.oneOrNone<PVF2>(query);
}

export function getByIdsNomen(idsNomen: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.pvf2 PVF2
        WHERE PVF2.cd_hab IN (${idsNomen.join(",")});
    `;
    return veglabDb.manyOrNone<PVF2>(query);
}

export function getByIdTaxo(idTaxo: number) {
    const query = `
        SELECT
            *
        FROM repos.pvf2 PVF2
        WHERE PVF2.cd_hab = ${idTaxo};
    `;
    return veglabDb.oneOrNone<PVF2>(query);
}

export function getByIdsTaxo(idsTaxo: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.pvf2 PVF2
        WHERE PVF2.cd_hab IN (${idsTaxo.join(",")});
    `;
    return veglabDb.manyOrNone<PVF2>(query);
}
