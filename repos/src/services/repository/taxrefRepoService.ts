import { veglabDb } from "../../config/database";
import { Taxref } from "../../models/repository/taxrefModel";

export function search(term: string) {
    term = term.replace(/ +/g, '%').replace(/-/g, '%');
    const query = `
        SELECT
            *
        FROM repos.taxref TAXREF
        WHERE unaccent(TAXREF.nom_complet) ILIKE unaccent('${term}%');
    `;

    return veglabDb.manyOrNone<Taxref>(query);
}

export function getByIdNomen(idNomen: number) {
    const query = `
        SELECT
            *
        FROM repos.taxref TAXREF
        WHERE TAXREF.cd_nom = ${idNomen};
    `;
    return veglabDb.oneOrNone<Taxref>(query);
}

export function getByIdsNomen(idsNomen: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.taxref TAXREF
        WHERE TAXREF.cd_nom IN (${idsNomen.join(",")});
    `;
    return veglabDb.manyOrNone<Taxref>(query);
}

export function getByIdTaxo(idTaxo: number) {
    const query = `
        SELECT
            *
        FROM repos.taxref TAXREF
        WHERE TAXREF.cd_nom = ${idTaxo};
    `;
    return veglabDb.oneOrNone<Taxref>(query);
}

export function getByIdsTaxo(idsTaxo: Array<number>) {
    const query = `
        SELECT
            *
        FROM repos.taxref TAXREF
        WHERE TAXREF.cd_nom IN (${idsTaxo.join(",")});
    `;
    return veglabDb.manyOrNone<Taxref>(query);
}
