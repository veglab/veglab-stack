import { Controller, Get, Path, Response, Route, Tags } from "tsoa";
import { ReverseGeocodeResponseModel } from '../../models/reverseGeocodeResponseModel';
import { ValidateErrorJSONModel } from '../../models/validateErrorJSONModel';
import { getCities, getCounties, getCountries, getRegions, getCode } from '../../services/iso3166';
import { Iso3166Model } from "../../models/iso3166Model";

@Tags("iso3166")
@Route("/iso3166")
export class ListCountriesController extends Controller{
    /**
     * Retrieves the specifications for countries supported in vegLab.
     */
    @Get("/countries")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async listCountries(): Promise<Array<Iso3166Model>|null> {
        return getCountries();
    }

    /**
     * Retrieves the specifications for regions supported in vegLab for a given country.
     */
    @Get("/regions/{countryCode}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async listRegions(
        @Path() countryCode: string,
    ): Promise<Array<Iso3166Model>|null> {
        return getRegions(countryCode);
    }

    /**
     * Retrieves the specifications for counties supported in vegLab for a given region.
     */
    @Get("/counties/{regionCode}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async listCounties(
        @Path() regionCode: string,
    ): Promise<Array<Iso3166Model>|null> {
        return getCounties(regionCode);
    }

    /**
     * Retrieves the cities in vegLab for a given county.
     */
    @Get("/cities/{countyCode}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async listCities(
        @Path() countyCode: string,
    ): Promise<Array<Iso3166Model>|null> {
        return getCities(countyCode);
    }

    /**
     * Retrieves the specifications for a given ISO3166-1 or ISO3166-2 code.
     */
    @Get("/code/{code}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async getCode(
        @Path() code: string,
    ): Promise<Array<Iso3166Model>|null> {
        return getCode(code);
    }
}
