import { Controller, Get, Path, Response, Route, Tags } from 'tsoa';
import { ValidateErrorJSONModel } from '../../models/validateErrorJSONModel';
import * as PVF2Service from '../../services/repository/pvf2RepoService';
import { PVF2 } from '../../models/repository/pvf2Model';

@Tags("repository")
@Route("/pvf2")
export class Pvf2RepoController extends Controller{

    /**
     * Searches for items matching the specified term.
     *
     * @param {string} term - The search term used to find matching items.
     */
    @Get("/term/{term}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    public async search(
        @Path() term: string
    ): Promise<Array<PVF2>> {
        return PVF2Service.search(term).then(results => {
            return results;
        });
    }

    /**
     * Get the item by its nomenclatural ID.
     *
     * @param {number} idNomen - The unique identifier for a nomenenclatural record to retrieve.
     */
    @Get("/nomen/{idNomen}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    public async getByIdNomen(@Path() idNomen: number): Promise<PVF2|null> {
        return PVF2Service.getByIdNomen(idNomen).then(result => {
            return result;
        });
    }

    /**
     * Get the items by a list of nomenclatural IDs.
     *
     * @param idsNomen A string containing a list of nomenclature IDs separated by commas. Each ID should be a numeric value.
     */
    @Get("/nomens/{idsNomen}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    public async getByIdsNomen(@Path() idsNomen: string): Promise<Array<PVF2>> {
        // Check that idsNomen only contains numbers and commas
        const isValidIdsNomen = /^(\d+,?)*$/.test(idsNomen);
        if (!isValidIdsNomen) {
            this.setStatus(422);
            return [];
        }
        const _idsNomen = idsNomen.split(',').map(id => parseInt(id, 10)) as Array<number>;

        return PVF2Service.getByIdsNomen(_idsNomen).then(results => {
            return results;
        });
    }

    /**
     * Get the item by its taxonomical ID.
     *
     * @param {number} idTaxo - The unique identifier for a taxonomical record to retrieve.
     */
    @Get("/taxo/{idTaxo}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    public async getByIdTaxo(@Path() idTaxo: number): Promise<PVF2|null> {
        return PVF2Service.getByIdTaxo(idTaxo).then(result => {
            return result;
        });
    }

    /**
     * Get the items by a list of taxonomical IDs.
     *
     * @param idsTaxo A string containing a list of taxonomical IDs separated by commas. Each ID should be a numeric value.
     */
    @Get("/taxos/{idsTaxo}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    public async getByIdsTaxo(@Path() idsTaxo: string): Promise<Array<PVF2>> {
        // Check that idsNomen only contains numbers and commas
        const isValidIdsTaxo = /^(\d+,?)*$/.test(idsTaxo);
        if (!isValidIdsTaxo) {
            this.setStatus(422);
            return [];
        }
        const _idsTaxo = idsTaxo.split(',').map(id => parseInt(id, 10)) as Array<number>;

        return PVF2Service.getByIdsTaxo(_idsTaxo).then(results => {
            return results;
        });
    }
}
