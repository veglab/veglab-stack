import { Controller, Get, Path, Post, Query, Response, Route, Tags } from "tsoa";
import { ReverseGeocodeResponseModel } from '../../models/reverseGeocodeResponseModel';
import { ValidateErrorJSONModel } from '../../models/validateErrorJSONModel';
import { geocode } from '../../services/geocoder';

@Tags("geocoder")
@Route("/geocoder")
export class GeocodeController extends Controller{
    /**
     * Geocode an entity (country, region, county, city, town, village, etc.) from a given term.
     *
     * Optionally filter through a country (ISO3166-1 code), a region or a county (ISO3166-2 codes).
     *
     * If results have a levenshtein distance of zero, only returns those results. Otherwise, returns the first 10 results.
     *
     * EPSG: 4326.
     *
     */
    @Get("/geocode/{term}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async reverse(
        @Path() term: string,
        @Query() countryCode?: string,
        @Query() regionCode?: string,
        @Query() countyCode?: string,
        @Query() level?: 'country'|'region'|'county'|'city',
    ): Promise<any|null> {
        return geocode(term, countryCode, regionCode, countyCode, level).then(results => {
            const zeroDistanceResults = [];
            for (const result of results) {
                if (result.levenshtein_distance === 0) {
                    zeroDistanceResults.push(result);
                }
            }
            return zeroDistanceResults.length > 0 ? zeroDistanceResults : results;
        });
    }
}
