import { Controller, Get, Path, Response, Route, Tags } from "tsoa";
import { ReverseGeocodeResponseModel } from '../../models/reverseGeocodeResponseModel';
import { ValidateErrorJSONModel } from '../../models/validateErrorJSONModel';
import { reverse } from '../../services/geocoder';

@Tags("geocoder")
@Route("/geocoder")
export class ReverseGeocodeController extends Controller{
    /**
     * Retrieves the country, region, county, city, postcode and geometry (geoJSON)
     * that INCLUDES a given point at lat/long.
     *
     * EPSG: 4326.
     *
     */
    @Get("/reverse/{lat}/{lng}")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response("200", "Ok")
    @Response("204", "No content")
    public async reverse(
        @Path() lat: number,
        @Path() lng: number
    ): Promise<ReverseGeocodeResponseModel|null> {
        return reverse(lat, lng);
    }
}
