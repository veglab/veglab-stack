import { Controller, Example, Get, Path, Post, Query, Response, Route, Tags } from "tsoa";
import { ReverseGeocodeResponseModel } from '../../models/reverseGeocodeResponseModel';
import { ValidateErrorJSONModel } from '../../models/validateErrorJSONModel';
import { validate } from '../../services/geocoder';
import { response } from "express";
import { ParameterErrorModel } from "../../models/parameterErrorModel";
import { GeocodeResponseModel } from "../../models/geocodeResponseModel";

@Tags("validator")
@Route("/validate")
export class ValidateController extends Controller {
    /**
     * Validate ISO3166 codes.
     *
     * Optionally provide country, region, county or city code to validate them.
     *
     * Returns 200 if the codes, all together, are valid.
     *
     * Returns 204 (no content) if the codes are not valid.
     *
     */
    @Example<any>([
        {
            "id": 6049,
            "name": "Les Eyzies",
            "level": "city",
            "countrycode": "FR",
            "country": "France",
            "regioncode": "FR-NAQ",
            "region": "Nouvelle-Aquitaine",
            "countycode": "FR-24",
            "county": "Dordogne",
            "citycode": "24172",
            "city": "Les Eyzies",
            "postcode": "24260"
        }
    ])
    @Get("/")
    @Response<ValidateErrorJSONModel>(400, "Parameter error")
    @Response<ValidateErrorJSONModel>(422, "Validation Failed")
    @Response<Error>(500, "Internal server error")
    @Response(200, "ISO3166 codes are valid", {a: "string"})
    @Response(204, "No content")
    public async validate(
        @Query() countryCode?: string,
        @Query() regionCode?: string,
        @Query() countyCode?: string,
        @Query() cityCode?: string,
    ): Promise<any|null> {
        return validate(countryCode, regionCode, countyCode, cityCode).then(results => {
            if (0 === results.length) {
                this.setStatus(204);
            }
            return results;
        });
    }
}
