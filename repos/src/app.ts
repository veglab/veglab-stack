import express, { Application, Request, Response, NextFunction } from 'express';
import { ValidateError } from 'tsoa';
import { ParameterError } from './errors/parameterError';
import { configDotenv } from 'dotenv';
import { RegisterRoutes } from '../build/routes';
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';
import morgan from 'morgan';
import * as rfs from 'rotating-file-stream';
import path from 'path';

const app: Application = express();
configDotenv({path: '/repos-api/.env'});
const port = process.env.GEO_API_EXTERNAL_PORT || '80';

app.use(express.json());


const accessLogStream = rfs.createStream('access.log', {
    interval: '1d', // rotate daily
    path: path.join(__dirname, '../log')
})

// setup the logger
app.use(morgan(':status :remote-addr - :remote-user [:date[clf]] ":url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"', {
    stream: accessLogStream,
    skip(req, res): boolean {
        return req.baseUrl.startsWith('/docs');
    }
}))

app.use(
    '/docs',
    swaggerUi.serve,
    async (req: Request, res: Response) => {
        return res.send(
            swaggerUi.generateHTML(await import ('../build/swagger.json'))
        );
    }
);

// Enable All CORS requests
// See https://expressjs.com/en/resources/middleware/cors.html for configuration
app.use(cors());
app.options('*', cors()); // enable pre-flight (OPTIONS)

RegisterRoutes(app);

app.use(function notFoundHandler(req: Request, res: Response) {
    res.status(404).send({
        message: "Not Found",
    });
});

app.use(function errorHandler(
    err: unknown,
    req: Request,
    res: Response,
    next: NextFunction
): Response | void {
    if (err instanceof ValidateError) {
        console.warn(`Caught Validation Error for ${req.path}:`, err.fields);
        return res.status(422).json({
            message: "Validation Failed",
            details: err?.fields,
        });
    }
    if (err instanceof ParameterError) {
        return res.status(400).json({
            message: err.message,
        })
    } else if (err instanceof Error) {
        return res.status(500).json({
            message: "Internal Server Error",
        });
    }

    next();
});

app.get('/', (req, res) => {
  res.redirect('/docs');
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
