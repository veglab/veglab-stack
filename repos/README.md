Repos API
=======

Ce service fournit les données et outils géographiques nécessaires au fonctionnement de vegLab, 
notamment :
- la diffusion des référentiels taxonomiques, syntaxonomiques et géographiques
- la recherche (géocodage et géocodage inversé) d'entités géographiques et leur rattachement aux niveaux supérieurs.
- la diffusion des trois niveaux supérieurs d'entités géographiques supportés dans le projet
  (pays, région , 'county' = département pour la France) au standard [ISO3166](https://en.wikipedia.org/wiki/ISO_3166),

Il s'agit d'une API Rest basée sur :  
- [ExpressJS](https://expressjs.com)
- [Typescript](https://www.typescriptlang.org/)
- [tsoa](https://tsoa-community.github.io/docs/)
- [SwaggerUI](https://swagger.io/tools/swagger-ui/)

Afin de faciliter les flux de données entre le backend, le front et cette API, les données géographiques 
sont hébergées dans la même base de données que les données d'occurrences, sous le schéma `geo`.  

Ces données sont principalement issues du projet [OpenStreetMap](https://www.openstreetmap.org/#map=16/44.4583/1.1992).  

Les référentiels taxonomiques et syntaxonomiques sont stockées sous le schéma `repos`;

## Notes
Nous souhaitons pouvoir rattacher chaque occurrence (géométrie de type point, polygone, polyligne) aux entités géographiques 
supérieures de type Pays, Région ou encore Département. Ceci afin d'organiser au mieux les informations géographiques et 
de permettre des recherches ciblées.  

Chaque pays dispose de niveaux administratifs qui lui sont propres et dont le nombre varie.  
Ces différents niveaux sont listés par la communauté OpenStreetMap. Dans 
[ce tableau](https://wiki.openstreetmap.org/wiki/Tag:boundary%3Dadministrative#11_admin_level_values_for_specific_countries), 
seuls les niveaux 2, 4, 6 et 8 sont pris en compte dans le projet vegLab sous les appellations génériques 
"country", "region", "county" et "city".  

Dans notre nomenclature,  
- "region" correspond à une "Région" en France et à un "Land" en Allemagne,
- "county" correspond donc à un "Département" en France et à un "Landkreise" en Allemagne,
- "city" correspond à une ville, un village, une commune, une municipalité ayant la plupart du temps un code postal et un code "commune".

## Autre
En développement, `tsoa` doit générer les fichiers `routes.js` et `swagger.json`.  
C'est la commande `dev` (voir `package.json`) qui se charge de cette tâche.  

La documentation officielle de tsoa mentionne l'utilisation de la lib `concurrently` conjointe à `nodemon` 
afin de générer puis surveiller tout changement de code :  
```text
concurrently "nodemon -x tsoa spec-and-routes" "nodemon"
```

Cette approche ne fonctionne pas pour nous, les fichiers sont souvent générés trop tard (le serveur est lancé plus rapidement).  
Nous utilisons une commande plus simple `tsoa spec && tsoa routes && nodemon` qui ne permet pas 
de répercuter les changements lors de la création/modification/suppression d'un contrôleur. 
Les routes ne sont pas mises à jour et il faut le faire manuellement : `yarn tsoa routes && yarn tsoa spec`.  
Notes que vous pouvez également lancer nodemon manuellement une fois le conteneur en route : 
```bash
docker exec -i vl-repos-api bash -c "yarn nodemon -x tsoa spec-and-routes"
```
