Mémo fonctions QGIS utiles pour gérer les intersections des différents niveaux administratifs.  


region_code  
'BE-WAL'  
'BE-VLG'  
'BE-BRU'  
```text
if (
	contains(geometry(get_feature('belgium_boundaries_simplified','ISO3166-2','BE-WAL')), $geometry),
	'BE-WAL',
	"region_cod"
	)
```

county_code  
BE-WNA  
BE-WLX  
BE-WLG  
BE-WHT  
BE-WBR  
BE-VWV  
BE-VOV  
BE-VLI  
BE-VBR  
BE-VAN  
```text
if (
	contains(geometry(get_feature('belgium_boundaries_simplified','ISO3166-2','BE-WNA')), $geometry),
	'BE-WNA',
	"county_cod"
	)
```
