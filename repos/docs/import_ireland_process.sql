-- CREATE DATABASE osm_irl
CREATE DATABASE osm_irl WITH OWNER osmuser;
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;

-- IMPORT OSM data
```bash
osmconvert ireland-and-northern-ireland-latest.osm.pbf -o=ireland-and-northern-ireland-latest.o5m
osm2pgsql -c -s ireland-and-northern-ireland-latest.o5m -d osm_irl -H localhost -P 7000 -U osmuser -W
```

-- CREATE TABLE FROM SELECTION (OSM polygons)
CREATE TABLE boundaries_irl AS
SELECT
    osm_id,
    admin_level,
    boundary,
    name,
    way AS geom
FROM osm_irl.public.planet_osm_polygon IRL
WHERE IRL.admin_level IN ('2', '4', '6', '8');

-- Check count entities
SELECT
    COUNT(name),
    IRL.admin_level
FROM boundaries_irl IRL
GROUP BY IRL.admin_level;

-- Merge geom + remove 'historic' boundaries (northern Ireland)
CREATE TABLE boundaries_irl_merged AS
SELECT DISTINCT admin_level,
                boundary,
                name,
                st_union(IRL.geom) as geom
FROM boundaries_irl IRL
WHERE IRL.admin_level IN ('2', '4', '6', '8')
  AND IRL.boundary != 'historic'
  AND IRL.name != 'Northern Ireland / Tuaisceart Éireann'
GROUP BY admin_level, boundary, name
;

SELECT
    *
FROM boundaries_irl_merged IRL;

-- INSERT country_code, region_code, county_code and city_code
ALTER TABLE boundaries_irl_merged
    ADD COLUMN country_code varchar(5);
ALTER TABLE boundaries_irl_merged
    ADD COLUMN region_code varchar(10);
ALTER TABLE boundaries_irl_merged
    ADD COLUMN county_code varchar(10);
ALTER TABLE boundaries_irl_merged
    ADD COLUMN city_code varchar(10);

-- SET COUNTRY ISO3166-1 code
UPDATE boundaries_irl_merged IRL
SET country_code = 'IE';

-- SET REGION codes
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-1' WHERE admin_level = '4' AND name = 'Dublin Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-2' WHERE admin_level = '4' AND name = 'Mid-East Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-3' WHERE admin_level = '4' AND name = 'Midlands Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-4' WHERE admin_level = '4' AND name = 'Mid-West Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-5' WHERE admin_level = '4' AND name = 'South-East Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-6' WHERE admin_level = '4' AND name = 'South-West Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-7' WHERE admin_level = '4' AND name = 'Border Region';
UPDATE boundaries_irl_merged IRL SET region_code = 'IE-8' WHERE admin_level = '4' AND name = 'West Region';

-- SET COUNTY ISO3166-2 codes
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-CW' WHERE admin_level = '6' AND name = 'County Carlow';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-CN' WHERE admin_level = '6' AND name = 'County Cavan';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-CE' WHERE admin_level = '6' AND name = 'County Clare';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-C' WHERE admin_level = '6' AND name = 'County Cork';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-DL' WHERE admin_level = '6' AND name = 'County Donegal';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-D' WHERE admin_level = '6' AND name = 'County Dublin';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-G' WHERE admin_level = '6' AND name = 'County Galway';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-KY' WHERE admin_level = '6' AND name = 'County Kerry';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-KE' WHERE admin_level = '6' AND name = 'County Kildare';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-KK' WHERE admin_level = '6' AND name = 'County Kilkenny';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-LS' WHERE admin_level = '6' AND name = 'County Laois';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-LM' WHERE admin_level = '6' AND name = 'County Leitrim';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-LK' WHERE admin_level = '6' AND name = 'County Limerick';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-LD' WHERE admin_level = '6' AND name = 'County Longford';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-LH' WHERE admin_level = '6' AND name = 'County Louth';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-MO' WHERE admin_level = '6' AND name = 'County Mayo';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-MH' WHERE admin_level = '6' AND name = 'County Meath';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-MN' WHERE admin_level = '6' AND name = 'County Monaghan';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-OY' WHERE admin_level = '6' AND name = 'County Offaly';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-RN' WHERE admin_level = '6' AND name = 'County Roscommon';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-SO' WHERE admin_level = '6' AND name = 'County Sligo';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-TA' WHERE admin_level = '6' AND name = 'County Tipperary';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-WD' WHERE admin_level = '6' AND name = 'County Waterford';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-WH' WHERE admin_level = '6' AND name = 'County Westmeath';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-WX' WHERE admin_level = '6' AND name = 'County Wexford';
UPDATE boundaries_irl_merged IRL SET county_code = 'IE-WW' WHERE admin_level = '6' AND name = 'County Wicklow';


-- REGION CODE FOR COUNTIES
-- Manuel
-- | Région              | Comtés                                   |
-- | ---                 | ---                                      |
-- | Dublin Region       | Dublin                                   |
-- | Mid-East Region     | Kildare, Meath, Wicklow                  |
-- | Midlands Region     | Laois, Longford, Offaly, Westmeath       |
-- | Mid-West Region     | Clare, Limerick, Tipperary               |
-- | South-East Region   | Carlow, Kilkenny, Waterford, Wexford     |
-- | South-West Region   | Cork, Kerry                              |
-- | Border Region       | Cavan, Donegal, Leitrim, Monaghan, Sligo |
-- | West Region         | Galway, Mayo, Roscommon                  |

-- REGION CODE FOR CITIES
WITH region_codes AS (
    SELECT
        IRL.*,
        IRL2.region_code AS regcode
    FROM boundaries_irl_merged IRL
             LEFT JOIN boundaries_irl_merged IRL2 ON ST_INTERSECTS(IRL.geom, IRL2.geom) AND IRL2.admin_level = '4'
    WHERE IRL.admin_level = '8'
)
UPDATE boundaries_irl_merged I
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = I.name AND I.admin_level = '8';

-- COUNTY CODE FOR CITIES
WITH county_codes AS (
    SELECT
        IRL.*,
        IRL2.county_code AS countycode
    FROM boundaries_irl_merged IRL
             LEFT JOIN boundaries_irl_merged IRL2 ON ST_INTERSECTS(IRL.geom, IRL2.geom) AND IRL2.admin_level = '6'
    WHERE IRL.admin_level = '8'
)
UPDATE boundaries_irl_merged D
SET county_code = county_codes.countycode
FROM county_codes
WHERE county_codes.name = D.name AND D.admin_level = '8';

-- EXPORT FORMAT
SELECT
    row_number() OVER (ORDER BY name) + 57673 AS id,
    IRL.name                               AS name,
    CASE WHEN admin_level = '2' THEN 'country'
         WHEN admin_level = '4' THEN 'region'
         WHEN admin_level = '6' THEN 'county'
         WHEN admin_level = '8' THEN 'city'
        END                               AS level,
    IRL.country_code                       AS country_code,
    IRL.region_code                        as region_code,
    IRL.county_code                        AS county_code,
    IRL.city_code                          AS city_code,
    NULL                                  AS postcode,
    st_transform(IRL.geom, 4326)::geometry AS the_geom_4326,
    st_asgeojson(IRL.geom)                 AS geojson,
    st_asgeojson(st_centroid(IRL.geom))    as geojson_centroid
FROM boundaries_irl_merged IRL;

-- SAVE the export format into SQL INSERT EXPORT_FORMAT_SP.sql file
```bash
sed -i 's/"MY_TABLE"/geo.boundary/g' EXPORT_IRL.sql
```

-- COPY the file into the shared_folder

-- RUN
```bash
docker exec -it vl-postgres bash -c "psql -d veglab_dev -U veglab_admin < /shared_folder/EXPORT_IRL.sql"
```
