-- CREATE DATABASE osm_de
CREATE DATABASE osm_de WITH OWNER osmuer;
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;

-- IMPORT OSM data
```bash
osmconvert netherlands-latest.osm.pbf -o=nl-latest.o5m
osm2pgsql -c -s nl-latest.o5m -d osm_de -H localhost -P 7000 -U osmuser -W
```

-- CREATE TABLE FROM SELECTION (OSM polygons)
CREATE TABLE boundaries_nl AS
SELECT
    osm_id,
    admin_level,
    boundary,
    name,
    way AS geom
FROM osm_nl.public.planet_osm_polygon NL
WHERE NL.admin_level IN ('2', '4', '8');

-- Check count entities
SELECT
    COUNT(name),
    NL.admin_level
FROM boundaries_nl NL
GROUP BY NL.admin_level;

-- INSERT country_code, region_code, county_code and city_code
ALTER TABLE boundaries_nl
    ADD COLUMN country_code varchar(5);
ALTER TABLE boundaries_nl
    ADD COLUMN region_code varchar(10);
ALTER TABLE boundaries_nl
    ADD COLUMN county_code varchar(10);
ALTER TABLE boundaries_nl
    ADD COLUMN city_code varchar(10);

-- DROP non administrative
SELECT DISTINCT boundary FROM boundaries_nl;
DELETE FROM boundaries_nl NL
WHERE NL.boundary IS NULL;

-- AGGREGATE geometries
--
WITH geom_union AS (
    SELECT DISTINCT
        BNL.admin_level,
        BNL.name,
        ST_UNION(BNL.geom) AS geom
    FROM boundaries_nl BNL
    GROUP BY BNL.admin_level, BNL.name
)
INSERT INTO boundaries_nl(osm_id, admin_level, boundary, name, geom, country_code, region_code, county_code, city_code)
SELECT DISTINCT
    -1 AS osm_id,
    NL.admin_level,
    boundary,
    NL.name,
    geom_union.geom AS geom,
    null AS country_code,
    null AS region_code,
    null AS county_code,
    null AS city_code
FROM boundaries_nl NL
         LEFT JOIN geom_union ON geom_union.admin_level = NL.admin_level AND geom_union.name = NL.name
;

-- REMOVE INITIAL (NON AGGREGATED) data
DELETE FROM boundaries_nl NL
WHERE NL.osm_id != -1;

-- SET COUNTRY ISO3166-1 code
UPDATE boundaries_NL NL
SET country_code = 'NL';

-- REGION and COUNTIES ISO3166-2 codes: manual
UPDATE boundaries_nl NL SET region_code = 'NL-DR' WHERE admin_level = '4' AND name = 'Drenthe';
UPDATE boundaries_nl NL SET region_code = 'NL-FL' WHERE admin_level = '4' AND name = 'Flevoland';
UPDATE boundaries_nl NL SET region_code = 'NL-FR' WHERE admin_level = '4' AND name = 'Fryslân';
UPDATE boundaries_nl NL SET region_code = 'NL-GE' WHERE admin_level = '4' AND name = 'Gelderland';
UPDATE boundaries_nl NL SET region_code = 'NL-GR' WHERE admin_level = '4' AND name = 'Groningen';
UPDATE boundaries_nl NL SET region_code = 'NL-LI' WHERE admin_level = '4' AND name = 'Limburg';
UPDATE boundaries_nl NL SET region_code = 'NL-NB' WHERE admin_level = '4' AND name = 'Noord-Brabant';
UPDATE boundaries_nl NL SET region_code = 'NL-NH' WHERE admin_level = '4' AND name = 'Noord-Holland';
UPDATE boundaries_nl NL SET region_code = 'NL-OV' WHERE admin_level = '4' AND name = 'Overijssel';
UPDATE boundaries_nl NL SET region_code = 'NL-UT' WHERE admin_level = '4' AND name = 'Utrecht';
UPDATE boundaries_nl NL SET region_code = 'NL-ZE' WHERE admin_level = '4' AND name = 'Zeeland';
UPDATE boundaries_nl NL SET region_code = 'NL-ZH' WHERE admin_level = '4' AND name = 'Zuid-Holland';

-- REGION CODE FOR COUNTIES
WITH region_codes AS (
    SELECT
        NL.*,
        NL2.region_code AS regcode
    FROM boundaries_nl NL
             LEFT JOIN boundaries_nl NL2 ON ST_INTERSECTS(NL.geom, NL2.geom) AND NL2.admin_level = '4'
    WHERE NL.admin_level = '6'
)
UPDATE boundaries_nl N
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = N.name AND N.admin_level = '6';

-- REGION CODE FOR CITIES
WITH region_codes AS (
    SELECT
        NL.*,
        NL2.region_code AS regcode
    FROM boundaries_nl NL
             LEFT JOIN boundaries_nl NL2 ON ST_INTERSECTS(NL.geom, NL2.geom) AND NL2.admin_level = '4'
    WHERE NL.admin_level = '8'
)
UPDATE boundaries_nl N
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = N.name AND N.admin_level = '8';


-- SET OSM IDS
-- SELECT MAX(id) FROM geo.boundary
-- Manually update oms_id from MAX(id) + 1 request result
-- UPDATE ids from CSV export and import into a new ids_names_nl table
WITH ids AS (SELECT osm_id AS _osm_id, admin_level AS _admin_level, name AS _name FROM ids_names_nl)
UPDATE boundaries_nl NL
SET osm_id = ids._osm_id
FROM ids
WHERE ids._admin_level::text = NL.admin_level::text AND ids._name = NL.name;

-- EXPORT FORMAT
SELECT
    osm_id                                AS id,
    NL.name                               AS name,
    CASE WHEN admin_level = '2' THEN 'country'
         WHEN admin_level = '4' THEN 'region'
         WHEN admin_level = '6' THEN 'county'
         WHEN admin_level = '8' THEN 'city'
        END                               AS level,
    NL.country_code                       AS country_code,
    NL.region_code                        as region_code,
    NL.county_code                        AS county_code,
    NL.city_code                          AS city_code,
    NULL                                  AS postcode,
    st_transform(NL.geom, 4326)::geometry AS the_geom_4326,
    st_asgeojson(NL.geom)                 AS geojson,
    st_asgeojson(st_centroid(NL.geom))    as geojson_centroid
FROM boundaries_nl NL;

-- SAVE the export format into SQL INSERT EXPORT_FORMAT_DE.sql file
```bash
sed -i 's/"MY_TABLE"/geo.boundary/g' EXPORT_NL.sql
```

-- COPY the file into the shared_folder

-- RUN
```bash
docker exec -it vl-postgres bash -c "psql -d veglab_dev -U veglab_admin < /shared_folder/EXPORT_NL.sql"
```

