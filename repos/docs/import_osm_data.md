Mémo pour importer les données depuis OSM.

Données disponibles sur [https://download.geofabrik.de](https://download.geofabrik.de).  

Installer `osmconvert` et `osmfilter`
```bash
sudo apt install osmctools
```

Convertir au format `o5m`
```bash
osmconvert belgium-latest.osm.pbf -o=belgium-latest.o5m
```
[25 secondes]

Filter les données pour ne conserver que les données "administratives"
```bash
osmfilter belgium-latest.o5m --keep="type=boundary and boundary=administrative" -o=belgium-latest-boundaries.o5m
```
[18 secondes]

Convertir au format `pbf` pour utilisation dans QGIS
```bash
osmconvert belgium-latest-boundaries.o5m -o=belgium-latest-boundaries.osm.pbf
```

Avec Osmosis:
```bash
osmosis --read-pbf-fast workers=2 "poland-latest-boundaries.o5m" --tf accept-relations boundary=administrative --write-pbf file="poland-latest-boundaries-osmosis1.o5m"

```

Avec  osm2pgsql
```bash
osm2pgsql -c -s poland-latest-boundaries.o5m -d osm -H localhost -P 7000 -U osmuser -W
```


QGIS : Traitement : "Exploser un champ HStore" > "other_tags"  

QGIS : ne conserver que les colonnes :  
- osm_id
- name
- boundary
- old_name
- type
- admin_level
- boundary
- place
- postal_code
- ISO3166-1
- ISO3166-2

QGIS :  
- Ajouter un champ "admin_vl_level"
  - -> "country" | "region" | "county" | "city"
- Ajouter un champ "country_code"
  - -> code ISO 3166-1 Alpha2 du pays (https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
- Ajouter un champ "country_name"
  - -> Nom du pays (en anglais ?)

- Ajouter un champ "region_code"
  - -> code ISO 3166-2
- Ajouter un champ "region_name"
  - -> Nom de la subdivision "région" pour vegLab

- Ajouter un champ "county_code"
  - -> code ISO 3166-2
- Ajouter un champ "county_name"
  - -> Nom de la subdivision "county" dans vegLab

- Ajouter un champ "postcode"
- Ajouter un champ "city_name"

Tip QGS: selection par localisation "Vecteur > Outil de recherche > Selection par localisation"

Le transfert de données depuis QGIS vers Postgres peut se faire de différentes manières.  
Le plus simple étant d'exporter les données au format CSV (les géométries sont alors converties au format WKB) avant de 
les importer dans Postgres. Dans ce cas, certaines communes sont mal encodées en WKB. Il peut être nécessaire de les 
importer manuellement en utilisant, par exemple, le plugin QGIS 'get WKT'.

La vérification des géométries peut se faire via la méthode `ST_IsValid' de PostGIS :  
```sql
CREATE TEMPORARY TABLE temp_check_boundaries_geom (id int, name varchar, is_valid boolean);

INSERT INTO temp_check_boundaries_geom
SELECT
    id,
    name,
    ST_ISVALID(the_geom_4326) as is_valid
FROM geo.boundary;

SELECT * FROM temp_check_boundaries_geom T
WHERE T.is_valid = true;


DROP TABLE temp_check_boundaries_geom;
```

# Correspondances entités

|             | Country       | Region                                                                               | County                                               | City                                                            | Date       | Status                                                                             |
|-------------|---------------|--------------------------------------------------------------------------------------|------------------------------------------------------|-----------------------------------------------------------------|------------|------------------------------------------------------------------------------------|
| France      | admin_level=2 | Région (admin_level=4)                                                               | Département (admin_level=6)                          | Commune (admin_level=8)                                         | 2023/08    | Ok France métropolitaine                                                           |
| Poland      | admin_level=2 | Voïvodies (admin_level=4)                                                            | Powiaty (admin_level=6)                              | Gminy (admin_level=7)                                           | 2023/03/20 | Ok mais reste les codes `county` à renseigner et rattacher les `city` aux `county` |
| Germany     | admin_level=2 | Bundesland (admin_level=4)                                                           | Landkreis / Kreis / kreisfreie Stadt (admin_level=6) | Stadt, Gemeinde (admin_level=8)                                 | 2024/03/19 | Ok Pas de code `county_code` -> les communes n'ont pas de `county_code` parent     |
| Belgium     | admin_level=2 | Regions (admin_level=4)                                                              | Province (admin_level=6)                             | Municipalité (admin_level=8)                                    | 2023/03/18 | Ok                                                                                 |
| Netherlands | admin_level=2 | Provincies (admin_level=4)                                                           | NA                                                   | Gemeenten (admin_level=8)                                       | 2023/03/19 | Ok métropole (pas de `city_code` ni de `county_code`)                              |
| Spain       | admin_level=2 | Comunidades autónomas (admin_level=4)                                                | Provincias (admin_level=6)                           | Municipios (admin_level=8)                                      | 2023/03/20 | Ok Hors Canaries                                                                   |
| Ireland     | admin_level=2 | - (Reserved for compatibility with UK states (eg Northern Ireland) ) (admin_level=4) | County (admin_level=6)                               | Borough & Town council, Dublin Postal Districts (admin_level=8) | 2024/11/25 | Ok ; les codes "régions" ont été attribué aléatoirement.                           |

Notes :  
- Pologne
  - Les données OSM présentent un niveau "administrative_religious" pour la clé "boundary". Il convient de supprimer ces entitiés
  - "La Pologne est divisée en 16 voïvodies, 379 powiaty (**dont 65 villes au statut de powiat**), et 2 478 gminy." source [wikipedia](https://fr.wikipedia.org/wiki/Organisation_territoriale_de_la_Pologne)
  - Seules les niveaux "country", "region" et "city" ont été intégrés
- Allemagne
  - Pas de county_code ! -> pas de county_code renseigné pour les communes 
  - voir https://wiki.openstreetmap.org/wiki/DE:Grenze 
  - code AGS téléchargés depuis : https://www.xrepository.de/details/urn:de:bund:destatis:bevoelkerungsstatistik:schluessel:ags
  - manque beaucoup (> 2000) city_code. A faire manuellement ?!
- Pays-Bas
  - Pas de county_code ! -> pas de county_code renseigné pour les communes
  - Pas de code commune unique officiel
- Espagne
  - Pas de code commune unique officiel
