Forcer le SRID :
```sql
ALTER TABLE geo.boundary ALTER COLUMN the_geom_4326 type geometry using ST_SETSRID(the_geom_4326, 4326);
```

Vérifier et simplifier les géométries :
```sql
SELECT
    ST_ISVALID(ST_MAKEVALID(st_simplifypreservetopology(the_geom_4326, 0.001))),
    ST_ISVALID(the_geom_4326),
    ST_MAKEVALID(st_simplifypreservetopology(the_geom_4326, 0.001))
FROM geo.boundary B;
```
