-- CREATE DATABASE osm_de
CREATE DATABASE osm_pl WITH OWNER osmuser;
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;

-- IMPORT OSM data
```bash
osmconvert poland-latest.osm.pbf -o=poland-latest.o5m
osm2pgsql -c -s poland-latest.o5m -d osm_pl -H localhost -P 7000 -U osmuser -W
```

-- CREATE TABLE FROM SELECTION (OSM polygons)
CREATE TABLE boundaries_pl AS
SELECT
    osm_id,
    admin_level,
    boundary,
    name,
    way AS geom
FROM osm_pl.public.planet_osm_polygon PL
WHERE PL.admin_level IN ('2', '4', '6', '7');

-- Check count entities
SELECT
    COUNT(name),
    PL.admin_level
FROM boundaries_pl PL
GROUP BY PL.admin_level;

-- INSERT country_code, region_code, county_code and city_code
ALTER TABLE boundaries_pl
    ADD COLUMN country_code varchar(5);
ALTER TABLE boundaries_pl
    ADD COLUMN region_code varchar(10);
ALTER TABLE boundaries_pl
    ADD COLUMN county_code varchar(10);
ALTER TABLE boundaries_pl
    ADD COLUMN city_code varchar(10);

-- DROP non administrative
SELECT DISTINCT PL.boundary FROM boundaries_pl PL;
DELETE FROM boundaries_pl PL
WHERE PL.boundary IN ('political', 'religious_administration', 'land_area') OR PL.boundary IS NULL;

-- AGGREGATE geometries
WITH geom_union AS (
    SELECT DISTINCT
        BPL.admin_level,
        BPL.name,
        ST_UNION(BPL.geom) AS geom
    FROM boundaries_pl BPL
    GROUP BY BPL.admin_level, BPL.name
)
INSERT INTO boundaries_pl(osm_id, admin_level, boundary, name, geom, country_code, region_code, county_code, city_code)
SELECT DISTINCT
    -1 AS osm_id,
    PL.admin_level,
    boundary,
    PL.name,
    geom_union.geom AS geom,
    null AS country_code,
    null AS region_code,
    null AS county_code,
    null AS city_code
FROM boundaries_pl PL
         LEFT JOIN geom_union ON geom_union.admin_level = PL.admin_level AND geom_union.name = PL.name
;

-- REMOVE INITIAL (NON AGGREGATED) data
DELETE FROM boundaries_pl PL
WHERE PL.osm_id != -1;

-- SET COUNTRY ISO3166-1 code
UPDATE boundaries_pl PL
SET country_code = 'PL';

-- REGION and COUNTIES ISO3166-2 codes: manual
UPDATE boundaries_pl PL SET region_code = 'PL-02' WHERE admin_level = '4' AND name = 'województwo dolnośląskie';
UPDATE boundaries_pl PL SET region_code = 'PL-04' WHERE admin_level = '4' AND name = 'województwo kujawsko-pomorskie';
UPDATE boundaries_pl PL SET region_code = 'PL-06' WHERE admin_level = '4' AND name = 'województwo łódzkie';
UPDATE boundaries_pl PL SET region_code = 'PL-08' WHERE admin_level = '4' AND name = 'województwo lubelskie';
UPDATE boundaries_pl PL SET region_code = 'PL-10' WHERE admin_level = '4' AND name = 'województwo lubuskie';
UPDATE boundaries_pl PL SET region_code = 'PL-12' WHERE admin_level = '4' AND name = 'województwo małopolskie';
UPDATE boundaries_pl PL SET region_code = 'PL-14' WHERE admin_level = '4' AND name = 'województwo mazowieckie';
UPDATE boundaries_pl PL SET region_code = 'PL-16' WHERE admin_level = '4' AND name = 'województwo opolskie';
UPDATE boundaries_pl PL SET region_code = 'PL-18' WHERE admin_level = '4' AND name = 'województwo podkarpackie';
UPDATE boundaries_pl PL SET region_code = 'PL-20' WHERE admin_level = '4' AND name = 'województwo podlaskie';
UPDATE boundaries_pl PL SET region_code = 'PL-22' WHERE admin_level = '4' AND name = 'województwo pomorskie';
UPDATE boundaries_pl PL SET region_code = 'PL-24' WHERE admin_level = '4' AND name = 'województwo śląskie';
UPDATE boundaries_pl PL SET region_code = 'PL-26' WHERE admin_level = '4' AND name = 'województwo świętokrzyskie';
UPDATE boundaries_pl PL SET region_code = 'PL-28' WHERE admin_level = '4' AND name = 'województwo warmińsko-mazurskie';
UPDATE boundaries_pl PL SET region_code = 'PL-30' WHERE admin_level = '4' AND name = 'województwo wielkopolskie';
UPDATE boundaries_pl PL SET region_code = 'PL-32' WHERE admin_level = '4' AND name = 'województwo zachodniopomorskie';

-- REGION CODE FOR level '7'
WITH region_codes AS (
    SELECT
        PL.*,
        PL2.region_code AS regcode
    FROM boundaries_pl PL
             LEFT JOIN boundaries_pl PL2 ON ST_INTERSECTS(PL.geom, PL2.geom) AND PL2.admin_level = '4'
    WHERE PL.admin_level = '7'
)
UPDATE boundaries_pl P
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = P.name AND P.admin_level = '7';

-- REGION CODE FOR level '6'
WITH region_codes AS (
    SELECT
        PL.*,
        PL2.region_code AS regcode
    FROM boundaries_pl PL
             LEFT JOIN boundaries_pl PL2 ON ST_INTERSECTS(PL.geom, PL2.geom) AND PL2.admin_level = '4'
    WHERE PL.admin_level = '6'
)
UPDATE boundaries_pl P
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = P.name AND P.admin_level = '6';

-- SET OSM IDS
-- SELECT MAX(id) FROM geo.boundary
-- Manually update oms_id from MAX(id) + 1 request result
-- UPDATE ids from CSV export and import into a new names_ids table
SELECT
    osm_id,
    admin_level,
    name
FROM boundaries_pl PL;

WITH ids AS (SELECT osm_id AS _osm_id, admin_level AS _admin_level, name AS _name FROM names_id_pl)
UPDATE boundaries_pl PL
SET osm_id = ids._osm_id
FROM ids
WHERE ids._admin_level::text = PL.admin_level::text AND ids._name = PL.name;

SELECT
    ST_SIMPLIFY(st_transform(geom, 4326), 0.01)
FROM boundaries_pl PL
WHERE PL.admin_level = '6';

-- EXPORT FORMAT
SELECT
    osm_id                                AS id,
    PL.name                               AS name,
    CASE WHEN admin_level = '2' THEN 'country'
         WHEN admin_level = '4' THEN 'region'
         WHEN admin_level = '6' THEN 'county'
         WHEN admin_level = '7' THEN 'city'
        END                               AS level,
    PL.country_code                       AS country_code,
    PL.region_code                        as region_code,
    PL.county_code                        AS county_code,
    PL.city_code                          AS city_code,
    NULL                                  AS postcode,
    st_transform(PL.geom, 4326)::geometry AS the_geom_4326,
    st_asgeojson(PL.geom)                 AS geojson,
    st_asgeojson(st_centroid(PL.geom))    as geojson_centroid
FROM boundaries_pl PL;

-- SAVE the export format into SQL INSERT EXPORT_FORMAT_DE.sql file
```bash
sed -i 's/"MY_TABLE"/geo.boundary/g' EXPORT_PL.sql
```

-- COPY the file into the shared_folder

-- RUN
```bash
docker exec -it vl-postgres bash -c "psql -d veglab_dev -U veglab_admin < /shared_folder/EXPORT_PL.sql"
```