-- CREATE TABLE FROM SELECTION (OSM polygons)
CREATE TABLE boundaries_sp AS
SELECT
    osm_id,
    admin_level,
    boundary,
    name,
    way AS geom
FROM osm_sp.public.planet_osm_polygon SP
WHERE SP.admin_level IN ('2', '4', '6', '8');

-- Check count entities
SELECT
    COUNT(name),
    SP.admin_level
FROM boundaries_sp SP
GROUP BY SP.admin_level;

-- INSERT country_code, region_code, county_code and city_code
ALTER TABLE boundaries_sp
    ADD COLUMN country_code varchar(5);
ALTER TABLE boundaries_sp
    ADD COLUMN region_code varchar(10);
ALTER TABLE boundaries_sp
    ADD COLUMN county_code varchar(10);
ALTER TABLE boundaries_sp
    ADD COLUMN city_code varchar(10);

-- DROP non administrative
SELECT DISTINCT boundary FROM boundaries_sp SP;

DELETE FROM boundaries_sp SP
WHERE SP.boundary != 'administrative' OR SP.boundary IS NULL;

-- AGGREGATE geometries
--
WITH geom_union AS (
    SELECT DISTINCT
        BSP.admin_level,
        BSP.name,
        ST_UNION(BSP.geom) AS geom
    FROM boundaries_sp BSP
    GROUP BY BSP.admin_level, BSP.name
)
INSERT INTO boundaries_sp(osm_id, admin_level, boundary, name, geom, country_code, region_code, county_code, city_code)
SELECT DISTINCT
    -1 AS osm_id,
    SP.admin_level,
    boundary,
    SP.name,
    geom_union.geom AS geom,
    null AS country_code,
    null AS region_code,
    null AS county_code,
    null AS city_code
FROM boundaries_sp SP
         LEFT JOIN geom_union ON geom_union.admin_level = SP.admin_level AND geom_union.name = SP.name
;

-- REMOVE INITIAL (NON AGGREGATED) data
DELETE FROM boundaries_sp SP
WHERE SP.osm_id != -1;

-- SET COUNTRY ISO3166-1 code
UPDATE boundaries_SP SP
SET country_code = 'SP';

-- REGION and COUNTIES ISO3166-2 codes: manual
UPDATE boundaries_sp SP SET region_code = 'ES-AN' WHERE admin_level = '4' AND name = 'Andalucía';
UPDATE boundaries_sp SP SET region_code = 'ES-AR' WHERE admin_level = '4' AND name = 'Aragón';
UPDATE boundaries_sp SP SET region_code = 'ES-AS' WHERE admin_level = '4' AND name = 'Asturias / Asturies';
UPDATE boundaries_sp SP SET region_code = 'ES-CN' WHERE admin_level = '4' AND name = 'Cantabria';
UPDATE boundaries_sp SP SET region_code = 'ES-CB' WHERE admin_level = '4' AND name = 'Castilla-La Mancha';
UPDATE boundaries_sp SP SET region_code = 'ES-CL' WHERE admin_level = '4' AND name = 'Castilla y León';
UPDATE boundaries_sp SP SET region_code = 'ES-CM' WHERE admin_level = '4' AND name = 'Catalunya';
UPDATE boundaries_sp SP SET region_code = 'ES-CT' WHERE admin_level = '4' AND name = 'Ceuta';
UPDATE boundaries_sp SP SET region_code = 'ES-CE' WHERE admin_level = '4' AND name = 'Comunidad de Madrid';
UPDATE boundaries_sp SP SET region_code = 'ES-EX' WHERE admin_level = '4' AND name = 'Comunitat Valenciana';
UPDATE boundaries_sp SP SET region_code = 'ES-GA' WHERE admin_level = '4' AND name = 'Euskadi';
UPDATE boundaries_sp SP SET region_code = 'ES-IB' WHERE admin_level = '4' AND name = 'Extremadura';
UPDATE boundaries_sp SP SET region_code = 'ES-RI' WHERE admin_level = '4' AND name = 'Galicia';
UPDATE boundaries_sp SP SET region_code = 'ES-MD' WHERE admin_level = '4' AND name = 'Gibraltar';
UPDATE boundaries_sp SP SET region_code = 'ES-ML' WHERE admin_level = '4' AND name = 'Illes Balears';
UPDATE boundaries_sp SP SET region_code = 'ES-MC' WHERE admin_level = '4' AND name = 'La Rioja';
UPDATE boundaries_sp SP SET region_code = 'ES-NC' WHERE admin_level = '4' AND name = 'Melilla';
UPDATE boundaries_sp SP SET region_code = 'ES-PV' WHERE admin_level = '4' AND name = 'Navarra - Nafarroa';
UPDATE boundaries_sp SP SET region_code = 'ES-VC' WHERE admin_level = '4' AND name = 'Región de Murcia';

UPDATE boundaries_sp SP SET region_code = 'ES-C' WHERE admin_level = '6' AND name = 'A Coruña';
UPDATE boundaries_sp SP SET region_code = 'ES-VI' WHERE admin_level = '6' AND name = 'Araba/Álava';
UPDATE boundaries_sp SP SET region_code = 'ES-AB' WHERE admin_level = '6' AND name = 'Albacete';
UPDATE boundaries_sp SP SET region_code = 'ES-A' WHERE admin_level = '6' AND name = 'Alacant / Alicante';
UPDATE boundaries_sp SP SET region_code = 'ES-AL' WHERE admin_level = '6' AND name = 'Almería';
UPDATE boundaries_sp SP SET region_code = 'ES-O' WHERE admin_level = '6' AND name = 'Asturias / Asturies';
UPDATE boundaries_sp SP SET region_code = 'ES-AV' WHERE admin_level = '6' AND name = 'Ávila';
UPDATE boundaries_sp SP SET region_code = 'ES-BA' WHERE admin_level = '6' AND name = 'Badajoz';
UPDATE boundaries_sp SP SET region_code = 'ES-B' WHERE admin_level = '6' AND name = 'Barcelona';
UPDATE boundaries_sp SP SET region_code = 'ES-BI' WHERE admin_level = '6' AND name = 'Bizkaia';
UPDATE boundaries_sp SP SET region_code = 'ES-BU' WHERE admin_level = '6' AND name = 'Burgos';
UPDATE boundaries_sp SP SET region_code = 'ES-CC' WHERE admin_level = '6' AND name = 'Cáceres';
UPDATE boundaries_sp SP SET region_code = 'ES-CA' WHERE admin_level = '6' AND name = 'Cádiz';
UPDATE boundaries_sp SP SET region_code = 'ES-S' WHERE admin_level = '6' AND name = 'Cantabria';
UPDATE boundaries_sp SP SET region_code = 'ES-CS' WHERE admin_level = '6' AND name = 'Castelló / Castellón';
UPDATE boundaries_sp SP SET region_code = 'ES-CR' WHERE admin_level = '6' AND name = 'Ciudad Real';
UPDATE boundaries_sp SP SET region_code = 'ES-CO' WHERE admin_level = '6' AND name = 'Córdoba';
UPDATE boundaries_sp SP SET region_code = 'ES-CU' WHERE admin_level = '6' AND name = 'Cuenca';
UPDATE boundaries_sp SP SET region_code = 'ES-SS' WHERE admin_level = '6' AND name = 'Gipuzkoa';
UPDATE boundaries_sp SP SET region_code = 'ES-GI' WHERE admin_level = '6' AND name = 'Girona';
UPDATE boundaries_sp SP SET region_code = 'ES-GR' WHERE admin_level = '6' AND name = 'Granada';
UPDATE boundaries_sp SP SET region_code = 'ES-GU' WHERE admin_level = '6' AND name = 'Guadalajara';
UPDATE boundaries_sp SP SET region_code = 'ES-H' WHERE admin_level = '6' AND name = 'Huelva';
UPDATE boundaries_sp SP SET region_code = 'ES-HU' WHERE admin_level = '6' AND name = 'Huesca';
UPDATE boundaries_sp SP SET region_code = 'ES-PM' WHERE admin_level = '6' AND name = 'Illes Balears';
UPDATE boundaries_sp SP SET region_code = 'ES-J' WHERE admin_level = '6' AND name = 'Jaén';
UPDATE boundaries_sp SP SET region_code = 'ES-LO' WHERE admin_level = '6' AND name = 'La Riojas';
--UPDATE boundaries_sp SP SET region_code = 'ES-GC' WHERE admin_level = '6' AND name = '';
UPDATE boundaries_sp SP SET region_code = 'ES-LE' WHERE admin_level = '6' AND name = 'León';
UPDATE boundaries_sp SP SET region_code = 'ES-L' WHERE admin_level = '6' AND name = 'Lleida';
UPDATE boundaries_sp SP SET region_code = 'ES-LU' WHERE admin_level = '6' AND name = 'Lugo';
UPDATE boundaries_sp SP SET region_code = 'ES-M' WHERE admin_level = '6' AND name = 'Comunidad de Madrid';
UPDATE boundaries_sp SP SET region_code = 'ES-MA' WHERE admin_level = '6' AND name = 'Málaga';
UPDATE boundaries_sp SP SET region_code = 'ES-MU' WHERE admin_level = '6' AND name = 'Región de Murcia';
UPDATE boundaries_sp SP SET region_code = 'ES-NA' WHERE admin_level = '6' AND name = 'Navarra - Nafarroa';
UPDATE boundaries_sp SP SET region_code = 'ES-OR' WHERE admin_level = '6' AND name = 'Ourense';
UPDATE boundaries_sp SP SET region_code = 'ES-P' WHERE admin_level = '6' AND name = 'Palencia';
UPDATE boundaries_sp SP SET region_code = 'ES-PO' WHERE admin_level = '6' AND name = 'Pontevedra';
UPDATE boundaries_sp SP SET region_code = 'ES-SA' WHERE admin_level = '6' AND name = 'Salamanca';
-- UPDATE boundaries_sp SP SET region_code = 'ES-TF' WHERE admin_level = '6' AND name = '';
UPDATE boundaries_sp SP SET region_code = 'ES-SG' WHERE admin_level = '6' AND name = 'Segovia';
UPDATE boundaries_sp SP SET region_code = 'ES-SE' WHERE admin_level = '6' AND name = 'Sevilla';
UPDATE boundaries_sp SP SET region_code = 'ES-SO' WHERE admin_level = '6' AND name = 'Soria';
UPDATE boundaries_sp SP SET region_code = 'ES-T' WHERE admin_level = '6' AND name = 'Tarragona';
UPDATE boundaries_sp SP SET region_code = 'ES-TE' WHERE admin_level = '6' AND name = 'Teruel';
UPDATE boundaries_sp SP SET region_code = 'ES-TO' WHERE admin_level = '6' AND name = 'Toledo';
UPDATE boundaries_sp SP SET region_code = 'ES-V' WHERE admin_level = '6' AND name = 'València / Valencia';
UPDATE boundaries_sp SP SET region_code = 'ES-VA' WHERE admin_level = '6' AND name = 'Valladolid';
UPDATE boundaries_sp SP SET region_code = 'ES-ZA' WHERE admin_level = '6' AND name = 'Zamora';
UPDATE boundaries_sp SP SET region_code = 'ES-Z' WHERE admin_level = '6' AND name = 'Zaragoza';

-- REGION CODE FOR COUNTIES
WITH region_codes AS (
    SELECT
        SP.*,
        SP2.region_code AS regcode
    FROM boundaries_sp SP
             LEFT JOIN boundaries_sp SP2 ON ST_INTERSECTS(SP.geom, SP2.geom) AND SP2.admin_level = '4'
    WHERE SP.admin_level = '6'
)
UPDATE boundaries_sp S
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = S.name AND S.admin_level = '6';


-- REGION CODE FOR CITIES
WITH region_codes AS (
    SELECT
        SP.*,
        SP2.region_code AS regcode
    FROM boundaries_sp SP
             LEFT JOIN boundaries_sp SP2 ON ST_INTERSECTS(SP.geom, SP2.geom) AND SP2.admin_level = '4'
    WHERE SP.admin_level = '8'
)
UPDATE boundaries_sp S
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = S.name AND S.admin_level = '8';


-- COUNTY CODE FOR CITIES
WITH county_codes AS (
    SELECT
        SP.*,
        SP2.county_code AS countycode
    FROM boundaries_sp SP
             LEFT JOIN boundaries_sp SP2 ON ST_INTERSECTS(SP.geom, SP2.geom) AND SP2.admin_level = '6'
    WHERE SP.admin_level = '8'
)
UPDATE boundaries_sp S
SET county_code = county_codes.countycode
FROM county_codes
WHERE county_codes.name = S.name AND S.admin_level = '8';

-- SET OSM IDS
SELECT
    osm_id,
    admin_level,
    name
FROM boundaries_sp SP;

WITH ids AS (SELECT osm_id AS _osm_id, admin_level AS _admin_level, name AS _name FROM names_ids_sp)
UPDATE boundaries_sp SP
SET osm_id = ids._osm_id
FROM ids
WHERE ids._admin_level::text = SP.admin_level::text AND ids._name = SP.name;

-- DROP Gibraltar and Portugal
SELECT * FROM boundaries_sp SP WHERE admin_level = '2';
DELETE FROM boundaries_sp SP WHERE SP.admin_level = '2' AND (name = 'Gibraltar' OR name = 'Portugal');

-- Check
SELECT
    ST_SIMPLIFY(ST_TRANSFORM(SP.geom, 4326), 0.05)
FROM boundaries_sp SP
WHERE SP.admin_level IN ('4', '6');

SELECT
    COUNT(name),
    SP.admin_level
FROM boundaries_sp SP
GROUP BY SP.admin_level;


-- EXPORT FORMAT
SELECT
    osm_id                                AS id,
    SP.name                               AS name,
    CASE WHEN admin_level = '2' THEN 'country'
         WHEN admin_level = '4' THEN 'region'
         WHEN admin_level = '6' THEN 'county'
         WHEN admin_level = '8' THEN 'city'
        END                               AS level,
    SP.country_code                       AS country_code,
    SP.region_code                        as region_code,
    SP.county_code                        AS county_code,
    SP.city_code                          AS city_code,
    NULL                                  AS postcode,
    st_transform(SP.geom, 4326)::geometry AS the_geom_4326,
    st_asgeojson(SP.geom)                 AS geojson,
    st_asgeojson(st_centroid(SP.geom))    as geojson_centroid
FROM boundaries_sp SP;

-- SAVE the export format into SQL INSERT EXPORT_FORMAT_SP.sql file
```bash
sed -i 's/"MY_TABLE"/geo.boundary/g' EXPORT_SP.sql
```

-- COPY the file into the shared_folder

-- RUN
```bash
docker exec -it vl-postgres bash -c "psql -d veglab_dev -U veglab_admin < /shared_folder/EXPORT_SP.sql"
```
