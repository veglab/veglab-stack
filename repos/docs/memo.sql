UPDATE boundaries_be BE
SET city_code = R."Code NIS"
FROM refnis_2019 R
WHERE R."Administratieve eenheden" = BE.name
  AND (city_code IS NULL OR city_code = '')
  AND admin_level = '8';

DELETE FROM boundaries_be
WHERE admin_level NOT IN ('2', '4', '6', '8');

DELETE FROM boundaries_be
WHERE admin_level IS NULL;

UPDATE boundaries_be
SET city_code = null
WHERE admin_level != '8';

SELECT
    BE.osm_id,
    BE.name,
    st_simplify(st_transform(geom, 4326), 0.0001)
FROM boundaries_be BE
WHERE (BE.city_code IS NULL OR BE.city_code = '') AND BE.admin_level = '8';

SELECT DISTINCT
    COUNT(osm_id),
    osm_id,
    admin_level,
    name,
    ST_UNION(geom)
FROM boundaries_be BE
WHERE BE.admin_level = '2'
GROUP BY osm_id, admin_level, name, geom

WITH be_geoms AS (
    SELECT
        ST_UNION(geom) as aggregate
    FROM boundaries_be
    WHERE admin_level = '2'
)
UPDATE boundaries_be
SET geom = BEG.aggregate
FROM be_geoms BEG
WHERE admin_level = '2' AND geom IS NULL


-- OK AGGREGATION GEOMETRIES
INSERT INTO boundaries_be(osm_id, admin_level, boundary, name, geom, city_code)
SELECT DISTINCT
    -1 AS osm_id,
    BE.admin_level,
    BE.boundary,
    BE.name,
    ST_UNION(BE.geom) AS geom,
    BE.city_code
FROM boundaries_be BE
WHERE BE.admin_level = '8'
GROUP BY BE.osm_id, BE.admin_level, BE.boundary, BE.name, BE.city_code;

DELETE FROM boundaries_be BE
WHERE BE.admin_level = '8'
  AND BE.osm_id != -1;

SELECT
    COUNT(name),
    BE.admin_level
FROM boundaries_be BE
GROUP BY BE.admin_level;

-- SET COUNTRY CODE
UPDATE boundaries_be BE
SET country_code = 'BE';

-- REGION and COUNTIES: manual

-- REGION CODE FOR CITIES
WITH region_codes AS (
    SELECT
        BE.*,
        BE2.region_code AS regcode
    FROM boundaries_be BE
             LEFT JOIN boundaries_be BE2 ON ST_INTERSECTS(BE.geom, BE2.geom) AND BE2.admin_level = '4'
    WHERE BE.admin_level = '8'
)
UPDATE boundaries_be B
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = B.name AND B.admin_level = '8';

-- COUNTY CODE FOR CITIES
WITH county_codes AS (
    SELECT
        BE.*,
        BE2.county_code AS countycode
    FROM boundaries_be BE
             LEFT JOIN boundaries_be BE2 ON ST_INTERSECTS(BE.geom, BE2.geom) AND BE2.admin_level = '6'
    WHERE BE.admin_level = '8'
)
UPDATE boundaries_be B
SET county_code = county_codes.countycode
FROM county_codes
WHERE county_codes.name = B.name AND B.admin_level = '8';

-- REGION CODE FOR COUNTIES
WITH region_codes AS (
    SELECT
        BE.*,
        BE2.region_code AS regcode
    FROM boundaries_be BE
             LEFT JOIN boundaries_be BE2 ON ST_INTERSECTS(BE.geom, BE2.geom) AND BE2.admin_level = '4'
    WHERE BE.admin_level = '6'
)
UPDATE boundaries_be B
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = B.name AND B.admin_level = '6';

-- BOUNDARIES FORMATTED TO EXPORT
SELECT
    osm_id AS id,
    BE.name AS name,
    CASE WHEN admin_level = '2' THEN 'country'
         WHEN admin_level = '4' THEN 'region'
         WHEN admin_level = '6' THEN 'county'
         WHEN admin_level = '8' THEN 'city'
        END AS level,
    BE.country_code AS country_code,
    BE.region_code as region_code,
    BE.county_code AS county_code,
    BE.city_code AS city_code,
    NULL AS postcode,
    st_transform(BE.geom, 4326)::geometry AS the_geom_4326,
    st_asgeojson(BE.geom) AS geojson,
    st_asgeojson(st_centroid(BE.geom)) as geojson_centroid
FROM boundaries_be BE
WHERE BE.osm_id IN (35509, 35510, 35511);

CREATE DATABASE osm_de WITH OWNER osmuser;

```
docker exec -it vl-postgres bash -c "psql -d veglab_dev -U veglab_admin < /shared_folder/BOUNDARIES_FORMATTED_TO_EXPORT.sql"
```
WITH ids AS (SELECT osm_id, name AS na FROM be_names_ids)
UPDATE boundaries_be
SET osm_id = ids.osm_id
FROM ids
WHERE ids.na = name;
