-- CREATE DATABASE osm_de
CREATE DATABASE osm_de WITH OWNER osmuser;
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;

-- IMPORT OSM data
```bash
osmconvert germany-latest.osm.pbf -o=germany-latest.o5m
osm2pgsql -c -s germany-latest.o5m -d osm_de -H localhost -P 7000 -U osmuser -W
```

-- CREATE TABLE FROM SELECTION (OSM polygons)
CREATE TABLE boundaries_de AS
SELECT
    osm_id,
    admin_level,
    boundary,
    name,
    way AS geom
FROM osm_de.public.planet_osm_polygon DE
WHERE DE.admin_level IN ('2', '4', '6', '8');

-- Check count entities
SELECT
    COUNT(name),
    DE.admin_level
FROM boundaries_de DE
GROUP BY DE.admin_level;

-- INSERT country_code, region_code, county_code and city_code
ALTER TABLE boundaries_de
    ADD COLUMN country_code varchar(5);
ALTER TABLE boundaries_de
    ADD COLUMN region_code varchar(10);
ALTER TABLE boundaries_de
    ADD COLUMN county_code varchar(10);
ALTER TABLE boundaries_de
    ADD COLUMN city_code varchar(10);

-- DROP non administrative
DELETE FROM boundaries_de DE
WHERE DE.boundary IS NULL;

-- AGGREGATE geometries
--
WITH geom_union AS (
    SELECT DISTINCT
        BDE.admin_level,
        BDE.name,
        ST_UNION(BDE.geom) AS geom
    FROM boundaries_de BDE
    GROUP BY BDE.admin_level, BDE.name
)
INSERT INTO boundaries_de(osm_id, admin_level, boundary, name, geom, country_code, region_code, county_code, city_code)
SELECT DISTINCT
    -1 AS osm_id,
    DE.admin_level,
    boundary,
    DE.name,
    geom_union.geom AS geom,
    null AS country_code,
    null AS region_code,
    null AS county_code,
    null AS city_code
FROM boundaries_de DE
         LEFT JOIN geom_union ON geom_union.admin_level = DE.admin_level AND geom_union.name = DE.name
;

-- REMOVE INITIAL (NON AGGREGATED) data
DELETE FROM boundaries_de DE
WHERE DE.osm_id != -1;

-- SET COUNTRY ISO3166-1 code
UPDATE boundaries_de DE
SET country_code = 'DE';

-- REGION and COUNTIES ISO3166-2 codes: manual
UPDATE boundaries_de DE SET region_code = 'DE-BW' WHERE admin_level = '4' AND name = 'Baden-Württemberg';
UPDATE boundaries_de DE SET region_code = 'DE-BY' WHERE admin_level = '4' AND name = 'Bayern';
UPDATE boundaries_de DE SET region_code = 'DE-BE' WHERE admin_level = '4' AND name = 'Berlin';
UPDATE boundaries_de DE SET region_code = 'DE-BB' WHERE admin_level = '4' AND name = 'Brandenburg';
UPDATE boundaries_de DE SET region_code = 'DE-HB' WHERE admin_level = '4' AND name = 'Bremen';
UPDATE boundaries_de DE SET region_code = 'DE-HH' WHERE admin_level = '4' AND name = 'Hamburg';
UPDATE boundaries_de DE SET region_code = 'DE-HE' WHERE admin_level = '4' AND name = 'Hessen';
UPDATE boundaries_de DE SET region_code = 'DE-MV' WHERE admin_level = '4' AND name = 'Mecklenburg-Vorpommern';
UPDATE boundaries_de DE SET region_code = 'DE-NI' WHERE admin_level = '4' AND name = 'Niedersachsen';
UPDATE boundaries_de DE SET region_code = 'DE-NW' WHERE admin_level = '4' AND name = 'Nordrhein-Westfalen';
UPDATE boundaries_de DE SET region_code = 'DE-RP' WHERE admin_level = '4' AND name = 'Rheinland-Pfalz';
UPDATE boundaries_de DE SET region_code = 'DE-SL' WHERE admin_level = '4' AND name = 'Saarland';
UPDATE boundaries_de DE SET region_code = 'DE-SN' WHERE admin_level = '4' AND name = 'Sachsen';
UPDATE boundaries_de DE SET region_code = 'DE-ST' WHERE admin_level = '4' AND name = 'Sachsen-Anhalt';
UPDATE boundaries_de DE SET region_code = 'DE-SH' WHERE admin_level = '4' AND name = 'Schleswig-Holstein';
UPDATE boundaries_de DE SET region_code = 'DE-TH' WHERE admin_level = '4' AND name = 'Thüringen';

-- REGION CODE FOR COUNTIES
WITH region_codes AS (
    SELECT
        DE.*,
        DE2.region_code AS regcode
    FROM boundaries_de DE
             LEFT JOIN boundaries_de DE2 ON ST_INTERSECTS(DE.geom, DE2.geom) AND DE2.admin_level = '4'
    WHERE DE.admin_level = '6'
)
UPDATE boundaries_de D
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = D.name AND D.admin_level = '6';

-- REGION CODE FOR CITIES
WITH region_codes AS (
    SELECT
        DE.*,
        DE2.region_code AS regcode
    FROM boundaries_de DE
             LEFT JOIN boundaries_de DE2 ON ST_INTERSECTS(DE.geom, DE2.geom) AND DE2.admin_level = '4'
    WHERE DE.admin_level = '8'
)
UPDATE boundaries_de D
SET region_code = region_codes.regcode
FROM region_codes
WHERE region_codes.name = D.name AND D.admin_level = '8';

-- COUNTY CODE FOR CITIES
WITH county_codes AS (
    SELECT
        DE.*,
        DE2.county_code AS countycode
    FROM boundaries_de DE
             LEFT JOIN boundaries_de DE2 ON ST_INTERSECTS(DE.geom, DE2.geom) AND DE2.admin_level = '6'
    WHERE DE.admin_level = '8'
)
UPDATE boundaries_de D
SET county_code = county_codes.countycode
FROM county_codes
WHERE county_codes.name = D.name AND D.admin_level = '8';


-- CREATE TABLE ags_fev2024 FROM csv file "ags_fev2024.csv": manual

-- UPDATE AGS CODES TO 7 Digit (heading 0 may be stripped during import)
-- UPDATE ags_fev2024 AGS
--SET ags = TO_CHAR(ags, 'fm0000000');

-- SET city_code
WITH codes AS (
    SELECT
        DE.name,
        AGS.ags
    FROM boundaries_de DE
             LEFT JOIN ags_fev2024 AGS ON AGS.name = DE.name
)
UPDATE boundaries_de DE
SET city_code = codes.ags
FROM codes
WHERE codes.name = DE.name;

-- SET OSM IDS
-- SELECT MAX(id) FROM geo.boundary
-- Manually update oms_id from MAX(id) + 1 request result
-- UPDATE ids from CSV export and import into a new names_ids table
SELECT
    osm_id,
    admin_level,
    name
FROM boundaries_de DE;
WITH ids AS (SELECT osm_id AS _osm_id, admin_level AS _admin_level, name AS _name FROM names_ids)
UPDATE boundaries_de DE
SET osm_id = ids._osm_id
FROM ids
WHERE ids._admin_level::text = DE.admin_level::text AND ids._name = DE.name;

-- EXPORT FORMAT
SELECT
    osm_id AS id,
    DE.name AS name,
    CASE WHEN admin_level = '2' THEN 'country'
         WHEN admin_level = '4' THEN 'region'
         WHEN admin_level = '6' THEN 'county'
         WHEN admin_level = '8' THEN 'city'
        END AS level,
    DE.country_code AS country_code,
    DE.region_code as region_code,
    DE.county_code AS county_code,
    DE.city_code AS city_code,
    NULL AS postcode,
    st_transform(DE.geom, 4326)::geometry AS the_geom_4326,
    st_asgeojson(DE.geom) AS geojson,
    st_asgeojson(st_centroid(DE.geom)) as geojson_centroid
FROM boundaries_de DE;

-- SAVE the export format into SQL INSERT EXPORT_FORMAT_DE.sql file
```bash
sed -i 's/"MY_TABLE"/geo.boundary/g' EXPORT_FORMAT_DE.sql
```

-- COPY the file into the shared_folder

-- RUN
```bash
docker exec -it vl-postgres bash -c "psql -d veglab_dev -U veglab_admin < /shared_folder/EXPORT_FORMAT_DE.sql"
```
