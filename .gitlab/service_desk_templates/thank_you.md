Merci pour votre demande de support !  

Nous avons bien pris en compte votre demande sous la référence [%{ISSUE_ID}](%{ISSUE_URL})

Description de la demande :  
> %{ISSUE_DESCRIPTION}

--

[Ne plus recevoir d'informations relatives à cette demande](%{UNSUBSCRIBE_URL})
