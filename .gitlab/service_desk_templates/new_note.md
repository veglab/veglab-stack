%{NOTE_TEXT}

--

Suivi de votre demande - référence [%{ISSUE_ID}](%{ISSUE_URL})

--

[Ne plus recevoir d'informations relatives à cette demande](%{UNSUBSCRIBE_URL})
