json <- fromJSON(req$postBody)

method <- json$dissimilarityMethod;
df <- data.frame(json$dataset$values)
colnames(df) <- c(json$dataset$taxons)
rownames(df) <- c(json$dataset$releves)
hcutVar <- json$hcut

uuid <- uuid(n = 1)

vegdist <- vegdist(df, method, binary=TRUE) # distance
vegsim <- abs(vegdist - 1)                  # absolute(distance - 1)

hc <- hclust(vegdist)
jpeg(paste("../data/output/hclust-dendro-",uuid,".jpg", sep = ""), width = 800, height = 600)
plot(hc)
if (!is.null(hcutVar)) { abline(h=hcutVar, col='red') }
dev.off()

vegsim_matrix <- as.matrix(vegsim)
jpeg(paste("../data/output/hclust-heatmap-",uuid,".jpg", sep = ""), width = 800, height = 600)
heatmap(vegsim_matrix)
dev.off()

graphDendro <- paste("hclust-dendro-",uuid,".jpg", sep = "")
graphHeatmap <- paste("hclust-heatmap-",uuid,".jpg", sep = "")

if (!is.null(hcutVar)) {
    ct <- cutree(hc, h = hcutVar)
    df <- data.frame(group=ct, releve=rownames(df))
    groups <- c(colnames(ct), ct)
    response <- list(graphDendro=graphDendro, graphHeatmap=graphHeatmap, groups=group_by(df, group))
} else {
    response <- list(graphDendro=graphDendro, graphHeatmap=graphHeatmap, groups=NA) # NA will be set to JSON null value
}
