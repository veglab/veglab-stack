VegLab stack
======
**Pile Docker du projet VegLab.**

- **traefik** : serveur [Traefik](https://traefik.io/)
- **api** : serveur Apache + PHP + Composer => application [Symfony](https://symfony.com/) / [API Platform](https://api-platform.com/)
- **postgres** : base de données principale, sous [postgreSQL](https://www.postgresql.org/)
- **repos-api** : application [ExpressJS](https://expressjs.com/) offrant une API (OpenAPI + Typescript via [TSOA](https://tsoa-community.github.io/docs/)) pour les référentiels taxonomiques, syntaxonomiques et géographiques
- **elasticsearch** : instance [Elasticsearch](https://www.elastic.co/fr/elastic-stack/) utilisée pour décharger la BDD principale en lecture (et :zap: accélérer :zap: les recherches)
- **client** : application front [Angular](https://angular.io/)
- **keycloak** : serveur d'identification SSO [Keycloak](https://www.keycloak.org/)
- **keycloak_mysql** : base de données [MySQL](https://www.mysql.com) utilisée par le SSO

# INSTALLATION
## Prérequis
- [Docker](https://docs.docker.com/engine/install/) >=20
- [Docker Compose plugin](https://docs.docker.com/compose/install/linux/#install-using-the-repository) >= 2
- [Git](https://git-scm.com/)

## Cloner le projet
Utiliser `git clone` pour cloner le projet en local ou sur un serveur de développement.
Les services 'client' (app front-end) et 'api' (app back-end) sont des sous-modules git de la stack.
Utiliser la commande `git submodule update --init --recursive` pour initialiser ces sous-modules.  

## Configuration du projet
### Variables d'environnement
Les environnements sont définis dans les fichiers `.env.{ENVIRONMENT}` et `.env.{ENVIRONMENT}.local` 
distincts selon l'hôte. Ces fichiers sont chargés avec cet ordre de priorité :
- `.env`
- `.env.local`
- `.env.dev`
- `.env.dev.local`
- `.env.preprod`
- `.env.preprod.local`
- `.env.prod`
- `.env.prod.local`

Ce qui signifie que le fichier `.env.local` surcharge le fichier `.env` et que le `.env.preprod` surcharge le `.env.dev.local` etc.
La plupart du temps, vous n'avez à gérer qu'un seul fichier d'environnement par serveur.  
Sur un serveur de préproduction, le `.env.preprod` par exemple.

Pour l'installation locale (développement), créer un fichier `.env.dev` à la racine du projet à partir du modèle `.env`.  

En environnement de développement, ajouter les adresses locales à votre fichier `hosts` à partir des variables définies dans 
la section `# HOSTS` de votre fichier `.env`:  
Sous Linux et macOS : `/etc/hosts`,  
Sous Windows : `C:\Windows\System32\drivers\etc\hosts`  

Exemple :
```text
127.0.0.1 proxy.dev.veglab.net  # interface Traefik
127.0.0.1 api.dev.veglab.net    # API
127.0.0.1 es.dev.veglab.net     # Elasticsearch
127.0.0.1 dev.veglab.net        # client Angular
127.0.0.1 sso.dev.veglab.net    # Keycloak
127.0.0.1 repos.dev.veglab.net  # Repos API
127.0.0.1 r.dev.veglab.net      # rplumber API
127.0.0.1 mail.dev.veglab.net   # Mailhog (dev)
```

### ElasticSearch
#### Certificats
Avant d'initialiser le service ES, il faut créer les certificats à l'aide de `docker-compose -f create-certs.yml up`.  
Un dossier `/certs` est créé à la racine du projet avec les certificats.

### API
Créer le fichier `/api/.env.local` à partir de `/api/.env` qui contient les variables d'exécution de l'API, notamment les lignes :
- `APP_ENV=dev` (`dev` ou `preprod`)
- `CORS_ALLOW_ORIGIN=^https?://your.host.com:?[0-9]*$`
- `FOS_ELASTICA_CLIENTS_DEFAULT_HOST=elasticsearch`
- `FOS_ELASTICA_PASSWORD=password`

Lancer a minima les conteneurs `api`, `postgres` et `elasticsearch` : 
```bash
docker compose up api elasticsearch
```

Installer les dépendances :
```bash
docker exec -it vl-api composer install
```

Création des bases de données : (! en attente de l'utilisation de Doctrine migration !)
```bash
docker exec -it vl-api php bin/console doctrine:database:create
docker exec -it vl-api php bin/console doctrine:schema:update --force
```

Création des index Elasticsearch :
```bash
docker exec -it vl-api php bin/console fos:elastica:create 
```

### Bases de données
#### PostgreSQL + PostGIS
Une base de données PostgreSQL est automatiquement créée lors de la création du service `vl-postgres`.

Extensions:
```
CREATE EXTENSION postgis;
CREATE EXTENSION unaccent;
CREATE EXTENSION fuzzystrmatch;
```

#### Elasticsearch
##### Accès restreint par mot de passe
Lancer, seul, le service `elasticsearch`:
```bash
docker compose down && docker-compose up elasticsearch
```

Générer les mots de passe avec :
```bash
docker exec -it elasticsearch elasticsearch-setup-passwords auto
```
Les mots de passe sont affichés dans le terminal.  
Le mdp pour "elastic" doit être renseigné pour la clé `ELASTIC_PASSWORD` du fichier `.env` du projet global.  
Ce mdp doit également être renseigné pour la clé `FOS_ELASTICA_PASSWORD` du fichier `.env.local` du projet `api`.  
En cas d'erreur, vérifiez que des données (anciens mots de passes, etc.) n'ont pas été stockés dans d'anciennes versions du conteneur. En dev/test, supprimer les conteneurs et volumes.  

##### Mappings
Pour créer les index avec le bon mapping, utiliser la commande fournie par FOS : 
```bash
docker compose up -d api
docker exec -it vl-api php bin/console fos:elastica:create --env=YOUR_ENV
```

## Dossier Media
Assurez-vous d'avoir un dossier `media/veglab/pdf` avec les droits en lecture/écriture dans `api/public`.

# Utilisation / mise à jour
- Créer les images : `docker-compose build` ; option `--no-cache` pour purger les caches
- Mise en route : `docker-compose up`

Lors de la mise à jour des sous-modules, il peut être utile de purger les volumes attachés. Par exemple, pour le client : `docker-compose rm -v client` avant de lancer un nouveau build `docker-compose build client`. 


# SSO - Keycloak
## Configuration
Ajouter le nom du realm 'veglab' à votre fichier .env (`KEYCLOAK_REALM=veglab`)

- Lancer le conteneur `keycloak`  
- Créer un nouveau Realm et utiliser le fichier `infra/keycloak/veglab-realm-clients-configuration.json` comme source pour importer toute la configuration nécessaire.  
- Créer au moins un utilisateur et assignez-lui les rôles `admin`, `realm-admin`, `ROLE_VEGLAB_ADMIN`
- Configurer un serveur mail pour le realm veglab (Settings > Email)

### Erreurs
Au lancement du service Keycloak, erreur "User with username 'xxx' already added to '/opt/jboss/keycloak/standalone/configuration/keycloak-add-user.json'"
- Stopper les conteneurs avec `docker compose down`
- Relancer les conteneurs `docker compose up`

# Documentation
Une application CMS [Grav](https://getgrav.org/) permet d'exposer la documentation utilisateur.  

## Configuration
La configuration de l'application n'est pas encore automatisée.  

Au premier déploiement du conteneur, il vous faut vous rendre sur la page de documentation (ex: docs.dev.veglab.net) qui 
vous invite à créer un compte administrateur.  

Le thème recommandé est [Vela](https://github.com/danzinger/grav-theme-vela).

Accédez à la page de configuration de Grav et
- ajoutez 'en, fr' aux langues prises en charge (menu Configuration > Système > Langues).
- indiquez titre: 'VegLab documentation', langue par défaut: 'fr', l'auteur et l'email dans Configuration > Système > Site
- créez une redirection '/' -> '/fr' dans Configuration > Système > Site


# Utilisations des verbes HTTP au sein de l'API Rest

> https://www.mscharhag.com/api-design/http-post-put-patch  
> POST requests create child resources at a server defined URI  
> PUT requests create or replace the resource at the client defined URI  
> PATCH requests update parts of the resource at the client defined URI

`GET`   => `table::read`
`POST`  => `table::create`
`PUT`   => `table::update`
`PATCH` => `table:identifications:update` (? A voir car dans ce cas, ce sont bien des entités "Identification" entières qui sont màj ; PATCH serait plus adapté à la mise à jour de champs particuliers)
